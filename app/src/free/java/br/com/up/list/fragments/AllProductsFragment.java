package br.com.up.list.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.up.list.activities.ProductDetail;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;
import br.com.up.list.utils.Search;

public class AllProductsFragment extends Fragment {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private View fragmentView;
    private View layoutSearch;
    private ViewGroup viewGroup;
    private ImageView iconCloseSearch;
    private EditText edtSearch;
    private ListView listAllProducts;

    public AllProductsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_all_products,
                container,
                false);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        // Banner
        AdView mAdView = fragmentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        iconCloseSearch = fragmentView.findViewById(R.id.icon_close_search);
        edtSearch = fragmentView.findViewById(R.id.edt_search);
        layoutSearch = fragmentView.findViewById(R.id.layout_search);
        viewGroup = fragmentView.findViewById(R.id.layout_frag_all_products);
        listAllProducts = fragmentView.findViewById(R.id.list_all_products);

        Toast msgException = Toast.makeText(
                context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        Cursor cursorAllProducts = sqLiteHelper.getAllProducts(msgException);
        if(cursorAllProducts.moveToFirst()) {
            listAllProducts.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_product,
                    cursorAllProducts,
                    new String[] {"NAME", "IMAGE_ID"},
                    new int[] {R.id.product_name, R.id.product_image},
                    0
            ));

            listAllProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtProductName = view.findViewById(R.id.product_name);
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("productId", id);
                    intent.putExtra("productName", txtProductName.getText().toString());
                    startActivity(intent);
                }
            });
        }

        edtSearch.addTextChangedListener(Search.filter(sqLiteHelper, 0, msgException, listAllProducts));
        iconCloseSearch.setOnClickListener(Search.close(viewGroup, layoutSearch));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_search:
                Search.open(viewGroup, layoutSearch);
        }
        return super.onOptionsItemSelected(item);
    }
}

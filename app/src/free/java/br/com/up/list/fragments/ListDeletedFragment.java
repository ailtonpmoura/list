package br.com.up.list.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.up.list.R;
import br.com.up.list.activities.MyProductListActivity;
import br.com.up.list.database.SQLiteHelper;

public class ListDeletedFragment extends Fragment {

    private Context context;
    private View fragmentView;
    private SQLiteHelper sqLiteHelper;
    private Toast msgException;

    public ListDeletedFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_deleted_list,
                container,
                false
        );
    }

    @Override
    public void onStart() {
        super.onStart();

        // Banner
        AdView mAdView = fragmentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        msgException = Toast.makeText(context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );
        Cursor cursor = sqLiteHelper.getLists(0, true, msgException);
        setAdapterAndClickListener(cursor);
    }

    private void updateCursor(ListView listDeleted, TextView listDeletedEmpty) {
        Cursor cursor = sqLiteHelper.getLists(0, true, msgException);
        CursorAdapter listAdapter = (CursorAdapter) listDeleted.getAdapter();
        listAdapter.changeCursor(cursor);

        // Se não houver registros no cursor, desabilita a lista e o adaptador
        if (!cursor.moveToFirst()) {
            listDeletedEmpty.setVisibility(View.VISIBLE);
            listDeleted.setAdapter(null);
        }
    }

    private void setAdapterAndClickListener(Cursor cursor) {

        final TextView listDeletedEmpty = fragmentView.findViewById(R.id.list_deleted_empty);
        final ListView listDeleted = fragmentView.findViewById(R.id.list_deleted);

        if (cursor.moveToFirst()) {
            listDeletedEmpty.setVisibility(View.GONE);

            listDeleted.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_list,
                    cursor,
                    new String[]{"NAME", "DATE"},
                    new int[]{R.id.list_name, R.id.list_date},
                    0
            ));

            listDeleted.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtListName = view.findViewById(R.id.list_name);
                    Intent intent = new Intent(context, MyProductListActivity.class);
                    intent.putExtra("listId", id);
                    intent.putExtra("isListDeleted", true);
                    intent.putExtra("listName", txtListName.getText().toString());
                    startActivity(intent);
                }
            });

            listDeleted.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent,
                                               View view, int position, final long id) {
                    PopupMenu popupMenu = new PopupMenu(context, view.findViewById(R.id.list_name));
                    popupMenu.inflate(R.menu.menu_list_deleted);
                    popupMenu.show();

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {

                            // Ativa a lista selecionada e atualiza o cursor
                            if (menuItem.getItemId() == R.id.menu_list_restore) {
                                sqLiteHelper.updateActiveList(id, 1);
                                updateCursor(listDeleted, listDeletedEmpty);
                                Toast.makeText(context, R.string.list_restore, Toast.LENGTH_LONG).show();

                            // Inicializa caixa de diálogo de deleção
                            } else if (menuItem.getItemId() == R.id.menu_list_delete){
                                onCreateDialog(id, listDeleted, listDeletedEmpty).show();
                            }
                        return true;
                        }
                    });
                    return true;
                }
            });

        } else {
            listDeletedEmpty.setVisibility(View.VISIBLE);
        }
    }

    // Caixa de diálogo de deleção da lista
    private Dialog onCreateDialog(final long listId, final ListView listDeleted,
                                  final TextView listDeletedEmpty) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.msg_deleted_list);

        // Deleta a lista e atualiza o cursor
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sqLiteHelper.deleteList(listId);
                updateCursor(listDeleted, listDeletedEmpty);
                Toast.makeText(context, R.string.list_removed, Toast.LENGTH_LONG).show();
            }
        });

        // Cancelar
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        return builder.create();
    }
}
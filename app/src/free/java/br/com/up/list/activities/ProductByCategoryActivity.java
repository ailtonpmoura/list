package br.com.up.list.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;
import br.com.up.list.utils.Search;

public class ProductByCategoryActivity extends Activity {

    private View layoutSearch;
    private ViewGroup viewGroup;
    private EditText edtSearch;
    private ImageView iconCloseSearch;
    private ListView productList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_by_category);

        // Banner
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);

        productList = findViewById(R.id.products_list);
        layoutSearch = findViewById(R.id.layout_search);
        viewGroup = findViewById(R.id.layout_activity_product_by_category);
        edtSearch = findViewById(R.id.edt_search);
        iconCloseSearch = findViewById(R.id.icon_close_search);

        final long categoryId = getIntent().getLongExtra("categoryId", 0);
        final long listId = getIntent().getLongExtra("listId", 0);
        final int productPosition = getIntent().getIntExtra("productPosition", 0);
        final String categoryName = getIntent().getStringExtra("categoryName");
        final String listName = getIntent().getStringExtra("listName");

        getActionBar().setTitle(categoryName);

        Toast msgException = Toast.makeText(
                this,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT);

        final Cursor cursorProducts = sqLiteHelper.getProducts(categoryId, msgException);

        productList.setAdapter(new SimpleCursorAdapter(
                getApplicationContext(),
                R.layout.item_custom_product,
                cursorProducts,
                new String[] {"NAME","IMAGE_ID"},
                new int[] {R.id.product_name, R.id.product_image},
                0
        ));

        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> adapterView,
                    View view,
                    int productPosition,
                    long productId) {
                Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                TextView txtProductName = view.findViewById(R.id.product_name);
                intent.putExtra("productId", productId);
                intent.putExtra("listId", listId);
                intent.putExtra("listName", listName);
                intent.putExtra("productName", txtProductName.getText().toString());

                // Verifica se a busca de produtos foi utilizada
                if (cursorProducts.getCount() == productList.getAdapter().getCount()) {
                    intent.putExtra("productPosition", productPosition);
                    intent.putExtra("categoryProductPosition", categoryId);
                }
                startActivity(intent);
            }
        });

        edtSearch.addTextChangedListener(Search.filter(sqLiteHelper, categoryId, msgException, productList));
        iconCloseSearch.setOnClickListener(Search.close(viewGroup, layoutSearch));

        if(productPosition != 0) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            productList.smoothScrollByOffset(productPosition);
                        }
                    },
                    500
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_search:
                Search.open(viewGroup, layoutSearch);
        }
        return super.onOptionsItemSelected(item);
    }
}

package br.com.up.list.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.text.NumberFormat;
import java.util.Locale;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MyProductListActivity extends Activity {

    private ListView productList;
    private TextView listProductEmpty;
    private TextView txtProductTotal;
    private TextView txtMyListCount;
    private TextView txtMyListCar;
    private SQLiteHelper sqLiteHelper;
    private StringBuilder msgShare;
    private String listName;
    private int productPosition;
    long categoryProductPosition;
    private long listId;
    private boolean isListDeleted;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    private void createMessageShare(Cursor myProductListCursor) {
        msgShare.append(myProductListCursor.getString(6));
        msgShare.append(" - ");
        msgShare.append(myProductListCursor.getString(2));
        msgShare.append(" ");
        msgShare.append(myProductListCursor.getString(3));
        msgShare.append("(s)\n");
    }

    private void calculateTotalAndMessageShare(
            Cursor myProductListCursor,
            Cursor countCarMyListCursor,
            TextView txtProductTotal,
            TextView txtMyListCount,
            TextView txtMyListCar) {

        msgShare = new StringBuilder();
        Double productAmount = 0.0;

        // Calcula o total da lista e monta mensagem de compartilhamento
        while(myProductListCursor.moveToNext()) {
            String productPriceTotal = myProductListCursor.getString(5).replace(",",".");
            productAmount += Double.parseDouble(productPriceTotal);
            createMessageShare(myProductListCursor);
        }
        msgShare.append("\n");
        msgShare.append("Obtenha o aplicativo List para Android:\n");
        msgShare.append("https://play.google.com/store/apps/details?id=br.com.up.list");

        // Insere o total da lista, a quantidade na lista e no carrinho
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        txtProductTotal.setText(numberFormat.format(productAmount.doubleValue()));
        txtMyListCount.setText(String.valueOf(myProductListCursor.getCount()));

        if(countCarMyListCursor.moveToFirst()) {
            txtMyListCar.setText(String.valueOf(countCarMyListCursor.getInt(0)));
        }
    }

    private void addOrRemoveCar(View view, TextView txtMyListCar,
                                ListView productList, long listId, long productId) {
        ImageView iconCar = view.findViewById(R.id.icon_car);
        Integer quantity = Integer.parseInt(txtMyListCar.getText().toString());

        if(iconCar.getDrawable() == null) {
            sqLiteHelper.updateProductIntoCar(listId, productId, R.mipmap.ic_add_shopping_cart_black_24dp);
            quantity += 1;
        } else {
            sqLiteHelper.updateProductIntoCar(listId, productId, 0);
            quantity -=  1;
        }

        Cursor MyProductListCursor = sqLiteHelper.myProductList(listId);
        CursorAdapter listAdapter = (CursorAdapter) productList.getAdapter();
        listAdapter.changeCursor(MyProductListCursor);
        txtMyListCar.setText(quantity.toString());

        // InterstitialAd
        int countAd;
        Cursor cursorCountAd = sqLiteHelper.getCountAd();
        if (cursorCountAd.moveToFirst()) {
            countAd = cursorCountAd.getInt(4);
            if (countAd >= 9) {
                if (mInterstitialAd.isLoaded()) {
                    sqLiteHelper.updateCountAd("COUNT_PRODUCT_CAR_AD",0);
                    mInterstitialAd.show();
                }
            } else {
                sqLiteHelper.updateCountAd("COUNT_PRODUCT_CAR_AD", ++countAd);
            }
        }
    }

    private void startActivity(long productId, String productName,
                               int productPosition, long categoryProductPosition) {
        Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
        intent.putExtra("productId", productId);
        intent.putExtra("listId", listId);
        intent.putExtra("productName", productName);
        intent.putExtra("listName", listName);
        intent.putExtra("productPosition", productPosition);
        intent.putExtra("categoryProductPosition", categoryProductPosition);
        intent.putExtra("isUpdate", true);
        startActivity(intent);
    }

    private void createPopUpMenu(final View view, final long productId) {
        PopupMenu popupMenu = new PopupMenu(
                getApplicationContext(),
                view.findViewById(R.id.desc_product_price));

        popupMenu.inflate(R.menu.menu_product_list);
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.menu_list_edit) {
                    TextView txtProductName = view.findViewById(R.id.product_name);
                    String productName = txtProductName.getText().toString();
                    startActivity(productId, productName, productPosition, categoryProductPosition);

                } else if (menuItem.getItemId() == R.id.menu_list_delete) {
                    sqLiteHelper.deleteProducList(productId, listId);
                    Cursor myProductListCursor = sqLiteHelper.myProductList(listId);
                    Cursor countCarMyListCursor = sqLiteHelper.countCarMyList(listId);

                    calculateTotalAndMessageShare(
                            myProductListCursor,
                            countCarMyListCursor,
                            txtProductTotal,
                            txtMyListCount,
                            txtMyListCar);

                    CursorAdapter listAdapter = (CursorAdapter) productList.getAdapter();
                    listAdapter.changeCursor(myProductListCursor);
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.product_excluded),
                            Toast.LENGTH_LONG)
                            .show();

                    // Se não houver registros no cursor, desabilita a lista e o adaptador
                    if (!myProductListCursor.moveToFirst()) {
                        listProductEmpty.setVisibility(View.VISIBLE);
                        productList.setAdapter(null);
                    }
                }
                return true;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_product_list);

        // Banner
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Intersticial
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3546715028063941/2764263511");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        sqLiteHelper = new SQLiteHelper(this);

        productList = findViewById(R.id.products_list);
        listProductEmpty = findViewById(R.id.list_product_empty);
        txtProductTotal = findViewById(R.id.product_price_total);
        txtMyListCount = findViewById(R.id.my_list_count);
        txtMyListCar = findViewById(R.id.my_list_car);
        ImageButton btn_to_category = findViewById(R.id.btn_to_category);
        TextView listDeletedProductEmpty = findViewById(R.id.list_deleted_product_empty);

        // Obtém o id da lista, o nome e verifica sua origem (ListFragment ou ListDeletedFragment)
        isListDeleted = getIntent().getBooleanExtra("isListDeleted", false);
        listId = getIntent().getLongExtra("listId", 0);
        productPosition = getIntent().getIntExtra("productPosition", 0);
        categoryProductPosition = getIntent().getLongExtra("categoryProductPosition", 0);
        listName = getIntent().getStringExtra("listName");
        getActionBar().setTitle(listName);

        // Busca os produtos da lista e a quantidade no carrinho
        Cursor myProductListCursor = sqLiteHelper.myProductList(listId);
        Cursor countCarMyListCursor = sqLiteHelper.countCarMyList(listId);

        // Calcula o total e monta a mensagem de compartilhamento
        calculateTotalAndMessageShare(
                myProductListCursor,
                countCarMyListCursor,
                txtProductTotal,
                txtMyListCount,
                txtMyListCar
        );

        // Se houver produtos na lista seta o adaptador
        if(myProductListCursor.moveToFirst()) {
            listProductEmpty.setVisibility(View.GONE);
            listDeletedProductEmpty.setVisibility(View.GONE);

            productList.setAdapter(new SimpleCursorAdapter(
                    getApplicationContext(),
                    R.layout.item_custom_product_detail,
                    myProductListCursor,
                    new String[]{"IMAGE_ID", "NAME", "PRICE", "AMOUNT", "UNITY", "TOTAL", "CAR"},
                    new int[]{R.id.product_image, R.id.product_name, R.id.product_price,
                            R.id.product_amount, R.id.product_unity, R.id.product_price_total,
                            R.id.icon_car},
                    0));

            // Oculta o botão de adicionar se a origem for ListDeleteFragment
            if (isListDeleted) {
                btn_to_category.setVisibility(View.GONE);

            // Seta os eventos da lista se a origem for ListFragment
            } else {
                productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            final View view, int position, long productId) {
                        addOrRemoveCar(view, txtMyListCar, productList, listId, productId);
                    }
                });

                productList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent,
                            View view, int position, long productId) {
                        createPopUpMenu(view, productId);
                        return true;
                    }
                });
            }

        // Se não houve produtos na lista, mostra o contúdo de acordo com a origem
        } else if (isListDeleted) {
            listDeletedProductEmpty.setVisibility(View.VISIBLE);
            listProductEmpty.setVisibility(View.GONE);
            btn_to_category.setVisibility(View.GONE);
        } else {
            listDeletedProductEmpty.setVisibility(View.GONE);
            listProductEmpty.setVisibility(View.VISIBLE);
        }

        btn_to_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                intent.putExtra("listId", listId);
                intent.putExtra("listName", listName);
                intent.putExtra("productPosition", productPosition);
                intent.putExtra("categoryProductPosition", categoryProductPosition);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        // Volta para MainActivity se a origem for ListFragment
        super.onBackPressed();
        if (!isListDeleted) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_product_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_share:
                if(!msgShare.toString().equals("")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, msgShare.toString());
                    Intent choserIntent = Intent.createChooser(
                            intent,
                            getResources().getString(R.string.send_list_to)
                    );
                    startActivity(choserIntent);

                    // InterstitialAd
                    int countAd;
                    Cursor cursorCountAd = sqLiteHelper.getCountAd();
                    if (cursorCountAd.moveToFirst()) {
                        countAd = cursorCountAd.getInt(5);
                        if (countAd >= 0) {
                            if (mInterstitialAd.isLoaded()) {
                                sqLiteHelper.updateCountAd("COUNT_LIST_SHARE_AD",0);
                                mInterstitialAd.show();
                            }
                        } else {
                            sqLiteHelper.updateCountAd("COUNT_LIST_SHARE_AD", ++countAd);
                        }
                    }

                } else {
                    Toast.makeText(
                            this,
                            getResources().getString(R.string.list_empty_share),
                            Toast.LENGTH_LONG
                    ).show();
                }
        }
        return super.onOptionsItemSelected(menuItem);
    }
}

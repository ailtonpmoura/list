package br.com.up.list.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.up.list.activities.MyProductListActivity;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;
import br.com.up.list.utils.Search;

public class ListFragment extends Fragment {

    private Context context;
    private View fragmentView;
    private TextView listEmpty;
    private ListView listSaved;
    private SQLiteHelper sqLiteHelper;
    private Toast msgException;
    private InterstitialAd mInterstitialAd;

    public ListFragment() {
    }

    private void startActivity(long listId, String listName) {
        Intent intent = new Intent(context, MyProductListActivity.class);
        intent.putExtra("listId", listId);
        intent.putExtra("listName", listName);
        startActivity(intent);
    }

    private void openToast(int idMsg) {
        Toast.makeText(context, getResources().getString(idMsg), Toast.LENGTH_SHORT).show();
    }

    private void inactiveList(long listId) {
        sqLiteHelper.updateActiveList(listId, 0);
        Cursor cursor = sqLiteHelper.getLists(1, true, msgException);
        CursorAdapter listAdapter = (CursorAdapter) listSaved.getAdapter();
        listAdapter.changeCursor(cursor);
        openToast(R.string.list_excluded);

        // Se não houver registros no cursor, desabilita a lista e o adaptador
        if(!cursor.moveToFirst()) {
            listEmpty.setVisibility(View.VISIBLE);
            listSaved.setAdapter(null);
        }
    }

    private void duplicateList(long listIdToDuplicity, View viewListItem) {
        long newListId;
        TextView txtListNameToDuplicity = viewListItem.findViewById(R.id.list_name);
        String listNameToDuplicity = txtListNameToDuplicity.getText().toString();
        saveOrUpdateList("createList", listNameToDuplicity, 0);

        Cursor listsActiviesCursor = sqLiteHelper.getLists(1, true, msgException);
        Cursor lastListCursor = sqLiteHelper.getLastList(msgException);
        Cursor productsListCursor = sqLiteHelper.getProductsList(listIdToDuplicity, msgException);

        if (lastListCursor.moveToFirst()) {
            newListId = lastListCursor.getLong(0);

            while (productsListCursor.moveToNext()) {
                long productId = productsListCursor.getLong(0);
                Cursor productDetailCursor = sqLiteHelper.getProductDetail(
                        listIdToDuplicity, productId, msgException);

                if (productDetailCursor.moveToFirst()) {
                    sqLiteHelper.addProductList(newListId, productId, msgException);
                    sqLiteHelper.addProductDetail(
                            Double.parseDouble(productDetailCursor.getString(4).replace(",", ".")),
                            productDetailCursor.getInt(5),
                            productDetailCursor.getString(6),
                            Double.parseDouble(productDetailCursor.getString(7).replace(",", ".")),
                            newListId,
                            productId);
                }
            }

            if (listsActiviesCursor.moveToFirst()) {
                CursorAdapter listSavedAdpter = (CursorAdapter) listSaved.getAdapter();
                listSavedAdpter.changeCursor(listsActiviesCursor);
                openToast(R.string.list_duplicity);
            }

            // InterstitialAd
            int countAd;
            Cursor cursorCountAd = sqLiteHelper.getCountAd();
            if (cursorCountAd.moveToFirst()) {
                countAd = cursorCountAd.getInt(3);
                if (countAd >= 1) {
                    if (mInterstitialAd.isLoaded()) {
                        sqLiteHelper.updateCountAd("COUNT_CREATE_LIST_AD",0);
                        mInterstitialAd.show();
                    }
                } else {
                    sqLiteHelper.updateCountAd("COUNT_CREATE_LIST_AD", ++countAd);
                }
            }
        }
    }

    private void createPopUpMenu(final View viewListItem, final long listId) {
        PopupMenu popupMenu = new PopupMenu(context, viewListItem.findViewById(R.id.list_name));
        popupMenu.inflate(R.menu.menu_list);
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_list_edit :
                        onCreateDialog("updateList", viewListItem, listId).show();
                        break;
                    case R.id.menu_list_duplicity :
                        duplicateList(listId, viewListItem);
                        break;
                    case R.id.menu_list_delete :
                        inactiveList(listId);
                        break;
                }
                return true;
            }
        });
    }

    private void setAdapterAndClickListener(Cursor cursor) {
        if(cursor.moveToFirst()) {
            listEmpty.setVisibility(View.GONE);

            listSaved.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_list,
                    cursor,
                    new String[] {"NAME", "DATE"},
                    new int[] {R.id.list_name, R.id.list_date},
                    0
            ));

            listSaved.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(
                        AdapterView<?> parent, View view, int position, long listId) {
                    TextView txtListName = view.findViewById(R.id.list_name);
                    String listName = txtListName.getText().toString();
                    startActivity(listId, listName);
                }
            });

            listSaved.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(
                        AdapterView<?> parent, View view, int position, long listId) {
                    createPopUpMenu(view, listId);
                    return true;
                }
            });

        } else {
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    private int saveOrUpdateList(String action, String listName, long listId) {
        if (action.equals("createList")) {
            sqLiteHelper.saveList(listName, Calendar.getInstance().getTimeInMillis());
            return R.string.list_created;
        } else {
            sqLiteHelper.updataeNameList(listId, listName);
            return R.string.list_updated;
        }
    }

    private boolean checkField(String listName) {
        if(listName.equals("")) {
            openToast(R.string.check_name_list);
            return false;
        }
        return true;
    }

    private void setTextDialog(TextView txtMessageDialog, int idMessage) {
        txtMessageDialog.setText(getResources().getString(idMessage));
    }

    private int configMessagesDialog(
            EditText edtListName, TextView txtMessageDialog, View viewListItem, String action) {
        if (action.equals("createList")) {
            setTextDialog(txtMessageDialog, R.string.name_list);
            return R.string.create;
        } else {
            setTextDialog(txtMessageDialog, R.string.new_name_list);
            edtListName.setText(((TextView) viewListItem.findViewById(R.id.list_name)).getText());
            return R.string.update_list;
        }
    }

    private Dialog onCreateDialog(final String action, View viewListItem, final long listId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View viewDialog = inflater.inflate(R.layout.create_list_dialog_layout, null);
        final TextView txtMessageDialog = viewDialog.findViewById(R.id.text_message_dialog);
        final EditText edtListName = viewDialog.findViewById(R.id.text_list_name);
        builder.setView(viewDialog);

        // Botão da caixa de diálogo de confirmação
        builder.setPositiveButton(
                configMessagesDialog(edtListName, txtMessageDialog, viewListItem, action),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String listName = edtListName.getText().toString();
                        if(checkField(listName)) {
                            int idMessageToast = saveOrUpdateList(action, listName, listId);
                            Cursor cursor = sqLiteHelper.getLists(1, true, msgException);

                            // Verifica se já existe um adapter na lista
                            if(listSaved.getAdapter() != null) {
                                CursorAdapter listAdapter = (CursorAdapter) listSaved.getAdapter();
                                listAdapter.changeCursor(cursor);
                            } else {
                                setAdapterAndClickListener(cursor);
                            }
                            openToast(idMessageToast);
                        }

                        // InterstitialAd
                        int countAd;
                        Cursor cursorCountAd = sqLiteHelper.getCountAd();
                        if (cursorCountAd.moveToFirst()) {
                            countAd = cursorCountAd.getInt(3);
                            if (countAd >= 1) {
                                if (mInterstitialAd.isLoaded()) {
                                    sqLiteHelper.updateCountAd("COUNT_CREATE_LIST_AD",1);
                                    mInterstitialAd.show();
                                }
                            } else {
                                sqLiteHelper.updateCountAd("COUNT_CREATE_LIST_AD", ++countAd);
                            }
                        }
                    }
                });

        // Botão do diálogo de negação
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        // Banner
        AdView mAdView = fragmentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Intersticial
        mInterstitialAd = new InterstitialAd(this.context);
        mInterstitialAd.setAdUnitId("ca-app-pub-3546715028063941/2764263511");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        listEmpty = fragmentView.findViewById(R.id.list_empty);
        listSaved = fragmentView.findViewById(R.id.list_saved);
        Button btnAddList = fragmentView.findViewById(R.id.btn_add_list);

        msgException = Toast.makeText(
                context, getResources().getString(R.string.exception_search), Toast.LENGTH_SHORT);

        Cursor cursor = sqLiteHelper.getLists(1, true, msgException);
        setAdapterAndClickListener(cursor);

        btnAddList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog("createList", null, 0).show();
            }
        });
    }

    private void start(String json, int extra) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, json);
        intent.putExtra("type", extra);

        Intent choserIntent = Intent.createChooser(intent,
                getResources().getString(R.string.send_list_pro));

        startActivity(choserIntent);

    }

    private void startActivityCategoryCreated(List<Object> list) {
        JSONArray jsonArray = new JSONArray(list);
        start(jsonArray.toString(),0);
    }

    private void startActivityListProduct(List<List<Map<String, List<Object>>>> list) {
        JSONArray jsonArray = new JSONArray(list);
        start(jsonArray.toString(),1);
    }

    private void startActivityListProductCreated(List<Object> list) {
        JSONArray jsonArray = new JSONArray(list);
        start(jsonArray.toString(),2);
    }

    private void startListProduct(Toast msgException) {
        try {
            Cursor cursorList = sqLiteHelper.getLists(1, false, msgException);

            List<Object> productDetail;
            List<Object> listDetail;

            Map<String, List<Object>> mapList;
            Map<String, List<Object>> mapProduct;

            List<Map<String, List<Object>>> listMapListProduct;
            List<List<Map<String, List<Object>>>> listOfListMapListProduct = new ArrayList<>();

            while (cursorList.moveToNext()) {
                mapProduct = new LinkedHashMap<>();
                mapList = new LinkedHashMap<>();
                listDetail = new ArrayList<>();
                listMapListProduct = new ArrayList<>();

                long listId = cursorList.getLong(0);
                String listName = cursorList.getString(1);
                String listDate = cursorList.getString(2);
                listDetail.add(listName);
                listDetail.add(listDate);
                mapList.put(String.valueOf(listId), listDetail);

                Cursor cursorProductList = sqLiteHelper.myProductList(listId);

                while (cursorProductList.moveToNext()) {
                    productDetail = new ArrayList<>();

                    String productId = String.valueOf(cursorProductList.getLong(0));

                    // D PRICE
                    productDetail.add(cursorProductList.getString(1));

                    // D AMOUNT
                    productDetail.add(cursorProductList.getDouble(2));

                    // D UNITY
                    productDetail.add(cursorProductList.getString(3));

                    // D TOTAL
                    productDetail.add(cursorProductList.getString(5));

                    // P NAME
                    productDetail.add(cursorProductList.getString(6));

                    // P IMAGE ID
                    productDetail.add(cursorProductList.getString(7));

                    // C ID
                    productDetail.add(cursorProductList.getLong(8));

                    mapProduct.put(productId, productDetail);
                }

                listMapListProduct.add(mapList);
                listMapListProduct.add(mapProduct);
                listOfListMapListProduct.add(listMapListProduct);
            }

            startActivityListProduct(listOfListMapListProduct);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Long startCreatedProduct(Toast msgException) {
        long time = 1000;

        try {
            Cursor cursorCreatedProducts = sqLiteHelper.getMyCreatedProducts(msgException);

            double max = cursorCreatedProducts.getCount();
            String percentage;
            int count = 1;

            while (cursorCreatedProducts.moveToNext()) {

                DecimalFormat decimalFormat = new DecimalFormat("0");
                percentage = decimalFormat.format((count * 100) / max);

                try {
                    List<Object> productCreateDetail = new ArrayList<>();

                    // Product Name
                    productCreateDetail.add(cursorCreatedProducts.getString(1));

                    // Product Image ID
                    productCreateDetail.add(cursorCreatedProducts.getString(2));

                    // Category ID
                    productCreateDetail.add(cursorCreatedProducts.getLong(4));

                    File imagefile = new File(cursorCreatedProducts.getString(2));
                    FileInputStream inputStream1 = new FileInputStream(imagefile);
                    ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
                    byte[] bytes;

                    Bitmap bm1 = BitmapFactory.decodeStream(inputStream1);
                    bm1.compress(Bitmap.CompressFormat.PNG, 100, outputStream1);
                    bytes = outputStream1.toByteArray();

                    if (bytes.length > 80000) {
                        FileInputStream inputStream2 = new FileInputStream(imagefile);
                        ByteArrayOutputStream outputStream2 = new ByteArrayOutputStream();
                        Bitmap bm2 = BitmapFactory.decodeStream(inputStream2);
                        if (bytes.length <= 200000) {
                            bm2.compress(Bitmap.CompressFormat.JPEG, 100, outputStream2);
                            bytes = outputStream2.toByteArray();
                        } else if (bytes.length <= 500000) {
                            bm2.compress(Bitmap.CompressFormat.JPEG, 50, outputStream2);
                            bytes = outputStream2.toByteArray();
                        } else if (bytes.length <= 800000) {
                            bm2.compress(Bitmap.CompressFormat.JPEG, 30, outputStream2);
                            bytes = outputStream2.toByteArray();
                        } else {
                            bm2.compress(Bitmap.CompressFormat.JPEG, 10, outputStream2);
                            bytes = outputStream2.toByteArray();
                        }
                    }

                    // Image Bytes
                    productCreateDetail.add(bytes);

                    String finalPercentage = percentage;
                    new android.os.Handler().postDelayed(new Runnable() {
                        public void run() {
                            startActivityListProductCreated(productCreateDetail);
                            Toast.makeText(context,
                                    "Enviando... ".concat(finalPercentage).concat("%"),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }, time);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                time += 1000;
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return time;
    }

    private void startCreatedCategory() {
        Toast msgException = Toast.makeText(context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT);

        Cursor cursorCategories = sqLiteHelper.getCategories(msgException);

        String[] categories = {"Mercearia", "Mercearia doce", "Laticínios",
                "Congelados e Resfriados","Bebidas", "Bebidas alcoólicas",
                "Higiene, Saúde e Beleza", "Limpeza", "Hortifruti"};

        List<Object> newCategories = new ArrayList<>();

        while (cursorCategories.moveToNext()) {
            String categoryName = cursorCategories.getString(1);
            if (!Arrays.asList(categories).contains(categoryName)) {
                newCategories.add(categoryName);
            }
        }

        startActivityCategoryCreated(newCategories);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_send_to_pro) {
            Toast.makeText(context,
                    getResources().getString(R.string.data_send),
                    Toast.LENGTH_LONG).show();

            Toast msgException = Toast.makeText(context,
                    getResources().getString(R.string.exception_search),
                    Toast.LENGTH_LONG);

            new android.os.Handler().postDelayed(new Runnable() {
                public void run() {
                    startCreatedCategory();

                    new android.os.Handler().postDelayed(new Runnable() {
                        public void run() {
                            Long time = startCreatedProduct(msgException);

                            if (time != null) {
                                new android.os.Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startListProduct(msgException);
                                    }
                                }, time + 2000);

                            } else {
                                msgException.show();
                            }
                        }
                    }, 2000);
                }
            }, 500);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_send_to_pro, menu);
    }
}

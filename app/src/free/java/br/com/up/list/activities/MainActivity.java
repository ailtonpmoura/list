package br.com.up.list.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.google.android.gms.ads.MobileAds;

import br.com.up.list.R;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.fragments.AllProductsFragment;
import br.com.up.list.fragments.CategoryFragment;
import br.com.up.list.fragments.CreateProductFragment;
import br.com.up.list.fragments.FavoriteFragment;
import br.com.up.list.fragments.ListDeletedFragment;
import br.com.up.list.fragments.ListFragment;
import br.com.up.list.fragments.MyCreatedProductsFragment;

public class MainActivity extends Activity {

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBar actionBar;
    private int currentPosition = 0;
    private SQLiteHelper sqLiteHelper;
    private Context context = this;

    private void setActionBarTitle(int position) {
        String[] titles = getResources().getStringArray(R.array.titles);
        actionBar.setTitle(titles[position]);
    }

    private void fragmentTransaction(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, "visible_fragment");
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    private void selectFragment(int position) {
        switch (position) {
            case 0:
                fragmentTransaction(new ListFragment());
                break;
            case 1:
                fragmentTransaction(new CategoryFragment());
                break;
            case 2:
                fragmentTransaction(new FavoriteFragment());
                break;
            case 3:
                fragmentTransaction(new AllProductsFragment());
                break;
            case 4:
                fragmentTransaction(new ListDeletedFragment());
                break;
            case 5:
                fragmentTransaction(new CreateProductFragment());
                break;
            case 6:
                fragmentTransaction(new MyCreatedProductsFragment());
                break;
            case 7:
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=br.com.up.list.pro");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, "ca-app-pub-3546715028063941~8115607671");

        sqLiteHelper = new SQLiteHelper(this);

        boolean initByFragmentCreatedProduct = getIntent().getBooleanExtra("productCreateOrUpdate", false);
        boolean initByMyCreatedProductsFragment = getIntent().getBooleanExtra("productEdit", false);

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList = findViewById(R.id.drawer_list);
        actionBar = getActionBar();

        if(savedInstanceState != null) {
            setActionBarTitle(savedInstanceState.getInt("position"));
        } else {
            if(initByFragmentCreatedProduct) {
                selectFragment(6);
                setActionBarTitle(6);
            } else if (initByMyCreatedProductsFragment){
                long productId = getIntent().getLongExtra("productId", 0);
                CreateProductFragment createProductFragment = new CreateProductFragment();
                createProductFragment.setProductId(productId);
                fragmentTransaction(createProductFragment);
                setActionBarTitle(7);
            } else {
                selectFragment(0);
            }
        }

        drawerList.setAdapter(new SimpleCursorAdapter(
                getApplicationContext(),
                R.layout.item_custom_drawer,
                sqLiteHelper.getListDrawer(),
                new String[] {"NAME","ICON"},
                new int[] {R.id.drawer_item_name, R.id.drawer_item_icon},
                0
        ));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectFragment(position);
                setActionBarTitle(position);
                drawerLayout.closeDrawer(findViewById(R.id.drawer_list));
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.open_drawer, R.string.close_drawer) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }
            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        drawerToggle.syncState();

        getFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        Fragment fragment = getFragmentManager().findFragmentByTag("visible_fragment");
                        if(fragment instanceof ListFragment) {
                            currentPosition = 0;
                        }
                        if(fragment instanceof CategoryFragment) {
                            currentPosition = 1;
                        }
                        if(fragment instanceof FavoriteFragment) {
                            currentPosition = 2;
                        }
                        if(fragment instanceof AllProductsFragment) {
                            currentPosition = 3;
                        }
                        if(fragment instanceof ListDeletedFragment) {
                            currentPosition = 4;
                        }
                        if(fragment instanceof CreateProductFragment) {
                            long productId = getIntent().getLongExtra("productId", 0);
                            if (productId != 0) {
                                currentPosition = 7;
                            } else {
                                currentPosition = 5;
                            }
                        }
                        if(fragment instanceof MyCreatedProductsFragment) {
                            currentPosition = 6;
                        }
                        setActionBarTitle(currentPosition);
                        drawerList.setItemChecked(currentPosition, true);
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt("position", currentPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

}

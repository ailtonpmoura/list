package br.com.up.list.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;

public class CategoryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        // Banner
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);

        ListView categoryList = findViewById(R.id.category_list);

        final long listId = getIntent().getLongExtra("listId", 0);
        final int productPosition = getIntent().getIntExtra("productPosition", 0);
        final long categoryProductPosition = getIntent().getLongExtra("categoryProductPosition", 0);
        final String listName = getIntent().getStringExtra("listName");

        Toast msgException = Toast.makeText(
                this,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        categoryList.setAdapter(new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                sqLiteHelper.getCategories(msgException),
                new String[] {"NAME"},
                new int[] {android.R.id.text1},
                0
        ));

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long categoryId) {
                Intent intent = new Intent(getApplicationContext(), ProductByCategoryActivity.class);
                TextView txtCategoryName = view.findViewById(android.R.id.text1);
                intent.putExtra("categoryId", categoryId);
                intent.putExtra("listId", listId);
                intent.putExtra("listName", listName);
                intent.putExtra("categoryName", txtCategoryName.getText().toString());
                if(categoryProductPosition == categoryId) {
                    intent.putExtra("productPosition", productPosition);
                }
                startActivity(intent);
            }
        });
    }
}

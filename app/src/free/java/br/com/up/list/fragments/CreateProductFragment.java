package br.com.up.list.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import br.com.up.list.activities.MainActivity;
import br.com.up.list.database.SQLiteHelper;
import static android.app.Activity.RESULT_OK;
import br.com.up.list.R;

public class CreateProductFragment extends Fragment {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private LayoutInflater inflater;
    private EditText edtProductName;
    private Spinner spnCategories;
    private ImageView img_create_product;
    private ImageButton btnUpdateCategory;
    private Button btnCreateOrUpdateProduct;
    private Bundle extras;
    private Bitmap imageBitmap;
    private String imageUri;
    private String dataUri;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_FOLDER = 2;
    private int requestCode = 0;
    private Toast msgException;
    private String CategoryName;
    private long productId = 0;
    private int categoryId = 0;
    private int positiveButton = 0;
    private int msgCategoryState = 0;
    private InterstitialAd mInterstitialAd;

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public CreateProductFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        long categoryPosition = spnCategories.getSelectedItemPosition();
        state.putLong("categoryPosition", categoryPosition);

        if (extras != null) {
            state.putBundle("extras", extras);
        }

        if (dataUri != null) {
            state.putString("uri", dataUri);
        }

        if(productId != 0) {
            state.putLong("productId", productId);
            state.putString("imageUri", imageUri);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            this.requestCode = requestCode;

            // Verifica qual foi a intencao requisitada para carregar a imagem
            // Caso a intencao seja a galeria de fotos, verifica o tamanho da imagem
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                img_create_product.setImageBitmap(imageBitmap);
            } else if (requestCode == REQUEST_IMAGE_FOLDER) {
                try {
                    dataUri = data.getData().toString();
                    Bundle bundle = new Bundle();
                    if (MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData()).getByteCount() / 1024 <= 30000){
                        imageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
                        img_create_product.setImageBitmap(imageBitmap);
                    } else  {
                        Toast.makeText(context, getResources().getString(R.string.error_image_full_size), Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    openToast(R.string.error_open_image);
                }
            }

        } else {
            openToast(R.string.error_open_image);
        }

    }

    private void startCreatedProductByMainActivity() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("productCreateOrUpdate", true);
        startActivity(intent);

        // InterstitialAd
        int countAd;
        Cursor cursorCountAd = sqLiteHelper.getCountAd();
        if (cursorCountAd.moveToFirst()) {
            countAd = cursorCountAd.getInt(2);
            if (countAd >= 3) {
                if (mInterstitialAd.isLoaded()) {
                    sqLiteHelper.updateCountAd("COUNT_PRODUCT_CREATED_AD",0);
                    mInterstitialAd.show();
                }
            } else {
                sqLiteHelper.updateCountAd("COUNT_PRODUCT_CREATED_AD", ++countAd);
            }
        }
    }

    private void openToast(int idMsg) {
        Toast.makeText(context, getResources().getString(idMsg), Toast.LENGTH_SHORT).show();
    }

    private boolean checkField(String field, int idMessage) {
        if(field.equals("")) {
            openToast(idMessage);
            return false;
        }
        return true;
    }

    private boolean findCategory(String selectedCategory) {
        boolean found = false;

        String[] baseCategories = {"Mercearia", "Mercearia doce", "Laticínios",
                "Congelados e Resfriados","Bebidas", "Bebidas alcoólicas",
                "Higiene, Saúde e Beleza", "Limpeza", "Hortifruti"};

        for(String category : baseCategories) {
            if(category.equals(selectedCategory)) {
                found = true;
            }
        }

        return found;
    }

    private Dialog onCreateDialog(final String action, String categoryName) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View viewDialog = inflater.inflate(R.layout.category_dialog_layout, null);
        final EditText edtCategoryName = viewDialog.findViewById(R.id.edt_category_name);
        final TextView txtCategoryName = viewDialog.findViewById(R.id.text_message_dialog_category);
        builder.setView(viewDialog);

        if(action.equals("create")) {
            positiveButton = R.string.create;
            msgCategoryState = R.string.new_category_created;
        } else {
            positiveButton = R.string.update;
            msgCategoryState = R.string.category_update;
            edtCategoryName.setText(categoryName);
            txtCategoryName.setText(R.string.update_category);
        }

        // Botão da caixa de diálogo de confirmação
        builder.setPositiveButton(
                positiveButton,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String categoryName = edtCategoryName.getText().toString();

                        if(checkField(categoryName, R.string.check_category_name)) {
                            if (!findCategory(categoryName)) {
                                if(action.equals("create")) {
                                    sqLiteHelper.insertNewCategory(categoryName);
                                } else {
                                    sqLiteHelper.updateCategory(categoryId, categoryName);
                                }
                            } else {
                                openToast(R.string.check_category_name_duplicity);
                                return;
                            }

                            Cursor cursorCategories = sqLiteHelper.getCategories(msgException);
                            CursorAdapter spnCategoriesAdapter = (CursorAdapter) spnCategories.getAdapter();
                            spnCategoriesAdapter.changeCursor(cursorCategories);
                            openToast(msgCategoryState);
                        }
                    }
                });

        // Botão do diálogo de negação
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

    private AdapterView.OnItemSelectedListener changeCategory() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CategoryName = ((Cursor) spnCategories.getItemAtPosition(position)).getString(1);
                if(findCategory(CategoryName)) {
                    btnUpdateCategory.setVisibility(View.GONE);
                } else {
                    btnUpdateCategory.setVisibility(View.VISIBLE);
                    categoryId = position + 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }

    // Configura os botões de atualização
    private void configureUpdateButtons() {
        btnCreateOrUpdateProduct.setText(R.string.update_product);
    }

    // Converte o uri de uma imagem em bitmap
    private void imageUriToBitmap(String imageUri) {
        try {
            FileInputStream stream = new FileInputStream(imageUri);
            imageBitmap = BitmapFactory.decodeStream(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void verifyImageUri() {
        if (imageUri.contains("/")) {
            imageUriToBitmap(imageUri);
            img_create_product.setImageBitmap(imageBitmap);
        }
    }

    // Configura o produto com seus dados
    private void configureUpdateProduct() {
        final Cursor productCursor = sqLiteHelper.getMyCreatedProduct(productId, msgException);

        if(productCursor.moveToFirst()) {
            String productName = productCursor.getString(1);
            categoryId = productCursor.getInt(3);
            final int positionCategory = categoryId - 1;
            imageUri = productCursor.getString(2);

            edtProductName.setText(productName);
            spnCategories.setSelection(positionCategory);

            verifyImageUri();
            configureUpdateButtons();
        }
    }

    // Carrega o estado atual da atividade
    private void loadState(Bundle savedInstanceState) {
        if(savedInstanceState.getLong("productId") != 0) {
            setProductId(savedInstanceState.getLong("productId"));
            imageUri = savedInstanceState.getString("imageUri");

            verifyImageUri();
            configureUpdateButtons();
        }

        long categoryPosition = savedInstanceState.getLong("categoryPosition");
        spnCategories.setSelection((int) categoryPosition);

        if (savedInstanceState.getBundle("extras") != null) {
            extras = savedInstanceState.getBundle("extras");
            imageBitmap = (Bitmap) extras.get("data");
            img_create_product.setImageBitmap(imageBitmap);
        }

        if (savedInstanceState.getString("uri") != null) {
            dataUri = savedInstanceState.getString("uri");
            Uri uri = Uri.parse(dataUri);
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                img_create_product.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Salva a imagem no caminho especificado do path
    private void saveImageStream(File imagePath) {
        OutputStream stream = null;
        try {
            stream = new FileOutputStream(imagePath);
            long size = (imageBitmap.getByteCount() / 1024);
            if (requestCode == 1) {
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            } else if (requestCode == 0 || requestCode == 2) {
                if (size > 25000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
                } else if (size > 20000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                } else if (size > 10000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream);
                } else if (size > 5000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                } else if (size > 2500) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                } else if (size > 2000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                } else if (size > 1000) {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 85, stream);
                } else {
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                }
            }
            stream.flush();
        } catch (Exception e) {
            openToast(R.string.error_save_product);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Cria um novo diretório e salva a imagem
    private File createDirectoryAndSaveImageStream(String productNamePath) {
        File imagePath = null;
        try {
            File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            imagePath = File.createTempFile(
                    productNamePath,
                    ".png",
                    storageDir
            );
            saveImageStream(imagePath);
        } catch (IOException e) {
            openToast(R.string.error_save_image);
            e.printStackTrace();
        }
        return imagePath;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);

        View fragmentView = inflater.inflate(
                R.layout.fragment_create_product,
                container,
                false
        );

        // Banner
        AdView mAdView = fragmentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Intersticial
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-3546715028063941/2764263511");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        ImageButton btnAddCategory = fragmentView.findViewById(R.id.btn_add_category);
        ImageButton btnAddProductImage = fragmentView.findViewById(R.id.btn_add_product_image);
        ImageButton btnAddProductImageFolder = fragmentView.findViewById(R.id.btn_add_product_image_folder);
        btnUpdateCategory = fragmentView.findViewById(R.id.btn_update_category);
        btnCreateOrUpdateProduct = fragmentView.findViewById(R.id.btn_create_update_product);
        edtProductName = fragmentView.findViewById(R.id.edt_create_product_name);
        spnCategories = fragmentView.findViewById(R.id.spn_categories);
        img_create_product = fragmentView.findViewById(R.id.img_create_product);

        msgException = Toast.makeText(context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        // Configura o adaptador de categorias
        spnCategories.setAdapter(new SimpleCursorAdapter(
                context,
                android.R.layout.simple_list_item_1,
                sqLiteHelper.getCategories(msgException),
                new String[]{"NAME"},
                new int[]{android.R.id.text1},
                0
        ));

        spnCategories.setOnItemSelectedListener(changeCategory());

        btnUpdateCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog("update", CategoryName).show();
            }
        });

        // Configura a atividade para atualização caso exista um id de produto
        if(productId != 0) {
            configureUpdateProduct();
        }

        // Carrega o estado atual da atividade caso seja destruida e recriada
        if (savedInstanceState != null) {
            loadState(savedInstanceState);
        }

        // Abre a caixa de diálogo para criar uma nova categoria
        btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateDialog("create", null).show();
            }
        });

        // Busca um aplicativo de câmera, caso seja encontrado, inicializa a câmera
        btnAddProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                } else {
                    openToast(R.string.error_open_camera);
                }

            }
        });

        btnAddProductImageFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent imageFolderIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                String[] mimeTypes = {"image/jpeg", "image/png"};
                imageFolderIntent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                if (imageFolderIntent.resolveActivity(context.getPackageManager()) != null) {
                    startActivityForResult(imageFolderIntent, REQUEST_IMAGE_FOLDER);
                }
            }
        });

        // Alterna a ação entre criar ou atualizar um produto dependendo do estado da atividade
        btnCreateOrUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productName = edtProductName.getText().toString();
                long idCategory = spnCategories.getSelectedItemId();
                File imagePath = null;
                String productNamePath;

                // Verifica se o nome do produto está preenchido
                if (checkField(productName, R.string.check_product_name)) {

                    // Converte "/" para "_" para não interferir no diretório
                    productName = productName.replace("/", "_");
                    productNamePath = productName.concat("_my_created_product_");

                    // Verifica se é uma atualização
                    if (productId != 0) {

                        // Atualiza o arquivo de imagem se existir uma
                        if (imageUri.contains("/")) {
                            saveImageStream(new File(imageUri));

                        // Atualiza a imagem padrão para a nova imagem se existir
                        } else if (imageBitmap != null) {
                            imagePath = createDirectoryAndSaveImageStream(productNamePath);
                        }

                        sqLiteHelper.updateMyProduct(
                                productId,
                                productName,
                                (imagePath != null ? imagePath.getAbsolutePath() : imageUri),
                                idCategory
                        );

                        openToast(R.string.product_update);
                        startCreatedProductByMainActivity();

                    // Salva novos produtos
                    } else {
                        // Verifica se não existe o nome do produto na categoria selecionada
                        Cursor myProductCursor = sqLiteHelper.findProductByNameAndCategory(productName, idCategory);
                        if (!myProductCursor.moveToFirst()) {

                            // Verifica se existe uma imagem para salva-la no diretório
                            if (imageBitmap != null) {
                                imagePath = createDirectoryAndSaveImageStream(productNamePath);
                            }

                            sqLiteHelper.insertNewProduct(
                                    productName,
                                    (imagePath != null ? imagePath.getAbsolutePath() : String.valueOf(R.mipmap.ic_crop_original_black_48dp)),
                                    idCategory
                            );

                            openToast(R.string.product_create);
                            startCreatedProductByMainActivity();

                        } else {
                            openToast(R.string.duplicate_product_category);
                        }
                    }
                }
            }
        });

        return fragmentView;
    }
}

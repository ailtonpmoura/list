package br.com.up.list.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.up.list.R;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "MERCADO";
    private static final int DB_VERSION = 6;
    private SQLiteDatabase database;
    private Cursor cursor;

    // Construtor
    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        this.database = database;
        updateDataBase(database, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        this.database = database;
        updateDataBase(database, oldVersion, newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        this.database = database;
        updateDataBase(database, oldVersion, newVersion);
    }

    private void updateDataBase(SQLiteDatabase database, int oldVersion, int newVersion) {
        if(oldVersion < 1) {

            // Cria a tabela de Categorias
            StringBuilder query_category = new StringBuilder();
            query_category.append("CREATE TABLE CATEGORY");
            query_category.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
            query_category.append("NAME TEXT)");
            database.execSQL(query_category.toString());

            String[] categories = {"Mercearia", "Mercearia doce", "Laticínios",
                    "Congelados e Resfriados","Bebidas", "Bebidas alcoólicas",
                    "Higiene, Saúde e Beleza", "Limpeza"};

            // Insere as categorias
            for(String category: categories) {
                insertCategory(category);
            }
        }

        if(oldVersion < 2) {
            Cursor listsSaved = null;
            Cursor productsDetailCursor = null;
            Cursor productsListCursor = null;
            Cursor favoriteProductsCursor = null;

            // Busca os produtos e listas salvos
            if (oldVersion == 1) {
                listsSaved = database.rawQuery(
                        "SELECT * FROM LIST", null);

                productsDetailCursor = database.rawQuery(
                        "SELECT * FROM PRODUCT_DETAIL", null);

                productsListCursor = database.rawQuery(
                        "SELECT * FROM PRODUCT_LIST", null);

                favoriteProductsCursor = database.rawQuery(
                        "SELECT * FROM PRODUCT WHERE FAVORITE = ?", new String[] {"1"});
            }

            insertCategory("Hortifruti");

            // Deleta a tabela PRODUCT, recria e insere os ítens novamente
            // Pata corrigir problema nos drawable
            deleteAndInsertAllProducts(favoriteProductsCursor);

            // Deleta a tabela LIST, recria e insere os ítens novamente
            deleteAndInsertListsSaved(listsSaved);

            // Deleta a tabela PRODUCT_DETAIL, recria e insere os ítens novamente
            deleteAndInsertProductDetail(productsDetailCursor);

            // Deleta a tabela de PRODUCT_LIST , recria e insere os ítens novamente
            insertListsAndProductsSaved(productsListCursor);

            // Deleta e insere todos os itens do menu novamente
            // Pata corrigir problema nos drawable
            deleteAndInsertAllItensDrawer();
        }

        if(oldVersion < 4) {
            // Deleta e insere todos os itens do menu novamente
            // Pata corrigir problema nos drawable
            deleteAndInsertAllItensDrawer();
        }

        if (oldVersion < 5) {
            StringBuilder query_count_ad = new StringBuilder();
            query_count_ad.append("CREATE TABLE COUNT_AD");
            query_count_ad.append("(_id INTEGER PRIMARY KEY, ");
            query_count_ad.append("COUNT_PRODUCT_LIST_AD NUMERIC, ");
            query_count_ad.append("COUNT_PRODUCT_CREATED_AD NUMERIC)");
            database.execSQL(query_count_ad.toString());
            insertCountAd();
        }

        if (oldVersion < 6) {
            String[] columns = new String[]{"COUNT_CREATE_LIST_AD", "COUNT_PRODUCT_CAR_AD", "COUNT_LIST_SHARE_AD", "COUNT_PRODUCT_FAVORITE_AD"};

            database.beginTransaction();
            for (String column : columns) {
                StringBuilder query_add_columns_ad = new StringBuilder();
                query_add_columns_ad.append("ALTER TABLE COUNT_AD ADD COLUMN ");
                query_add_columns_ad.append(column).append(" NUMERIC");
                database.execSQL(query_add_columns_ad.toString());
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    private void dropAndCreateTableProduct() {
        database.execSQL("DROP TABLE IF EXISTS PRODUCT");

        StringBuilder query_product = new StringBuilder();
        query_product.append("CREATE TABLE PRODUCT");
        query_product.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query_product.append("NAME TEXT, ");
        query_product.append("IMAGE_ID INTEGER, ");
        query_product.append("FAVORITE NUMERIC, ");
        query_product.append("CATEGORY_ID INTEGER, ");
        query_product.append("FOREIGN KEY(CATEGORY_ID) REFERENCES CATEGORY(_id))");
        database.execSQL(query_product.toString());
    }

    private void dropAndCreateTableList() {
        database.execSQL("DROP TABLE IF EXISTS LIST");

        StringBuilder query_list = new StringBuilder();
        query_list.append("CREATE TABLE LIST");
        query_list.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query_list.append("NAME TEXT, ");
        query_list.append("DATE NUMERIC, ");
        query_list.append("ACTIVE NUMERIC)");
        database.execSQL(query_list.toString());
    }

    private void dropAndCreateTableProductDetail() {
        database.execSQL("DROP TABLE IF EXISTS PRODUCT_DETAIL");

        StringBuilder query_product_detail = new StringBuilder();
        query_product_detail.append("CREATE TABLE PRODUCT_DETAIL");
        query_product_detail.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query_product_detail.append("PRICE REAL, ");
        query_product_detail.append("AMOUNT REAL, ");
        query_product_detail.append("UNITY TEXT, ");
        query_product_detail.append("TOTAL REAL, ");
        query_product_detail.append("CAR Numeric, ");
        query_product_detail.append("LIST_ID INTEGER, ");
        query_product_detail.append("PRODUCT_ID INTEGER, ");
        query_product_detail.append("FOREIGN KEY(LIST_ID) REFERENCES PRODUCT_LIST(LIST_ID) ON DELETE CASCADE, ");
        query_product_detail.append("FOREIGN KEY(PRODUCT_ID) REFERENCES PRODUCT_LIST(PRODUCT_ID) ON DELETE CASCADE)");
        database.execSQL(query_product_detail.toString());
    }

    private void dropAndCreateTableProductList() {
        database.execSQL("DROP TABLE IF EXISTS PRODUCT_LIST");

        StringBuilder query_product_list = new StringBuilder();
        query_product_list.append("CREATE TABLE PRODUCT_LIST");
        query_product_list.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query_product_list.append("LIST_ID INTEGER, ");
        query_product_list.append("PRODUCT_ID INTEGER, ");
        query_product_list.append("FOREIGN KEY(LIST_ID) REFERENCES LIST(_id) ON DELETE CASCADE, ");
        query_product_list.append("FOREIGN KEY(PRODUCT_ID) REFERENCES PRODUCT(_id) ON DELETE CASCADE)");
        database.execSQL(query_product_list.toString());
    }

    private void deleteAndInsertAllProducts(Cursor favoriteProductsCursor) {
        List<Integer> favoriteProducts = new ArrayList<>();

        if (favoriteProductsCursor != null) {
            while (favoriteProductsCursor.moveToNext()) {
                favoriteProducts.add(favoriteProductsCursor.getInt(0));
            }
        }

        dropAndCreateTableProduct();
        insertAllProducts();

        if (favoriteProducts.size() > 0) {
            for (int id : favoriteProducts) {
                database.execSQL(
                        "UPDATE PRODUCT SET FAVORITE = ? WHERE _ID = ?",
                        new String[] {"1", String.valueOf(id)});
            }
        }
    }

    private void deleteAndInsertListsSaved(Cursor listsCursor) {
        List<Map> lists = new ArrayList<>();

        if (listsCursor != null) {
            while (listsCursor.moveToNext()) {
                Map<String, Object> listMap = new HashMap<>();
                listMap.put("listName", listsCursor.getString(1));
                listMap.put("listCreated", listsCursor.getLong(2));
                listMap.put("listActive", listsCursor.getInt(3));
                lists.add(listMap);
            }
        }

        dropAndCreateTableList();

        for (Map listMap : lists) {
            ContentValues values = new ContentValues();
            values.put("NAME", (String) listMap.get("listName"));
            values.put("DATE", (long) listMap.get("listCreated"));
            values.put("ACTIVE", (int) listMap.get("listActive"));
            database.insert("LIST", null, values);
        }
    }

    private void deleteAndInsertProductDetail(Cursor productsDetailCursor) {
        List<Map> productsDetail = new ArrayList<>();

        if (productsDetailCursor != null) {
            while (productsDetailCursor.moveToNext()) {
                Map<String, Object> detailMap = new HashMap<>();
                detailMap.put("price", productsDetailCursor.getDouble(1));
                detailMap.put("amount", productsDetailCursor.getDouble(2));
                detailMap.put("unity", productsDetailCursor.getString(3));
                detailMap.put("total", productsDetailCursor.getDouble(4));
                detailMap.put("car", productsDetailCursor.getInt(5));
                detailMap.put("listId", productsDetailCursor.getLong(6));
                detailMap.put("productId", productsDetailCursor.getLong(7));
                productsDetail.add(detailMap);
            }
        }

        dropAndCreateTableProductDetail();

        for (Map detailMap : productsDetail) {
            ContentValues values = new ContentValues();
            values.put("PRICE", (Double) detailMap.get("price"));
            values.put("AMOUNT", (Double) detailMap.get("amount"));
            values.put("UNITY", (String) detailMap.get("unity"));
            values.put("TOTAL", (Double) detailMap.get("total"));
            values.put("CAR", (int) detailMap.get("car") == 0 ?
                    (int) detailMap.get("car") : R.mipmap.ic_add_shopping_cart_black_24dp);
            values.put("LIST_ID", (long) detailMap.get("listId"));
            values.put("PRODUCT_ID", (long) detailMap.get("productId"));
            database.insert("PRODUCT_DETAIL", null, values);
        }
    }

    // Insere os produtos e listas salvos
    private void insertListsAndProductsSaved(Cursor productsListCursor){
        List<Map> listsProducts = new ArrayList<>();

        if (productsListCursor != null) {
            while (productsListCursor.moveToNext()) {
                Map<String, Object> listProductMap = new HashMap<>();
                listProductMap.put("listId", productsListCursor.getLong(1));
                listProductMap.put("productId", productsListCursor.getLong(2));
                listsProducts.add(listProductMap);
            }
        }

        dropAndCreateTableProductList();

        for (Map listProductMap : listsProducts) {
            ContentValues values = new ContentValues();
            values.put("LIST_ID", (long) listProductMap.get("listId"));
            values.put("PRODUCT_ID", (long) listProductMap.get("productId"));
            database.insert("PRODUCT_LIST", null, values);
        }

    }

    // Atualiza os ítens do Drawer
    private void deleteAndInsertAllItensDrawer() {
        database.execSQL("DROP TABLE IF EXISTS LIST_DRAWER");

        StringBuilder query_list_drawer = new StringBuilder();
        query_list_drawer.append("CREATE TABLE LIST_DRAWER");
        query_list_drawer.append("(_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query_list_drawer.append("NAME TEXT, ");
        query_list_drawer.append("ICON INTEGER)");
        database.execSQL(query_list_drawer.toString());

        insertListDrawer("Minhas Listas", R.mipmap.ic_toc_white_24dp);
        insertListDrawer("Categorias", R.mipmap.ic_group_work_white_24dp);
        insertListDrawer("Produtos Favoritos", R.mipmap.ic_favorite_white_24dp);
        insertListDrawer("Todos os Produtos", R.mipmap.ic_view_list_white_24dp);
        insertListDrawer("Listas excluídas", R.mipmap.ic_delete_white_24dp);
        insertListDrawer("Criar produto", R.mipmap.ic_add_a_photo_white_24dp);
        insertListDrawer("Produtos Criados", R.mipmap.ic_burst_mode_white_24dp);
        insertListDrawer("List Pro", R.mipmap.ic_shop_two_white_24dp);
    }

    private void insertCategory(String name) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        database.insert("CATEGORY", null, values);
    }

    public void insertNewCategory(String name) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        getWritableDatabase().insert("CATEGORY", null, values);
    }

    private void insertCountAd() {
        ContentValues values = new ContentValues();
        values.put("COUNT_PRODUCT_LIST_AD", 0);
        values.put("COUNT_PRODUCT_CREATED_AD", 0);
        database.insert("COUNT_AD", null, values);
    }

    public void updateCategory(int categoryId, String categoryName) {
        ContentValues values = new ContentValues();
        values.put("NAME", categoryName);
        getWritableDatabase().update(
                "CATEGORY",
                values,
                "_id = ?",
                new String[] {String.valueOf(categoryId)});
    }

    private void insertProduct(String name, int image, long categoryId) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("IMAGE_ID", image);
        values.put("FAVORITE", 0);
        values.put("CATEGORY_ID", categoryId);
        database.insert("PRODUCT", null, values);
    }

    public void insertNewProduct(String name, String image, long categoryId) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("IMAGE_ID", image);
        values.put("FAVORITE", 0);
        values.put("CATEGORY_ID", categoryId);
        getWritableDatabase().insert("PRODUCT", null, values);
    }

    public void updateMyProduct(long productId, String productName, String image, long categoryId) {
        ContentValues values = new ContentValues();
        values.put("NAME", productName);
        values.put("IMAGE_ID", image);
        values.put("CATEGORY_ID", categoryId);
        getWritableDatabase().update(
                "PRODUCT",
                values,
                "_id = ?",
                new String[] {String.valueOf(productId)});
    }

    private void insertListDrawer(String name, int icon) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("ICON", icon);
        database.insert("LIST_DRAWER", null, values);
    }

    public Cursor myProductList(long listId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("P._id, ");
        query.append("replace(printf('%.2f', D.PRICE),'.',',') AS PRICE, ");
        query.append("D.AMOUNT, D.UNITY, D.CAR, ");
        query.append("replace(printf('%.2f', D.TOTAL),'.',',') AS TOTAL, ");
        query.append("P.NAME, P.IMAGE_ID, P.CATEGORY_ID ");
        query.append("FROM PRODUCT_LIST LP ");
        query.append("INNER JOIN PRODUCT P ON P._id = LP.PRODUCT_ID ");
        query.append("NATURAL JOIN PRODUCT_DETAIL D ");
        query.append("WHERE LP.LIST_ID = ?");
        return getReadableDatabase().rawQuery(
                query.toString(),
                new String[] {Long.toString(listId)}
        );
    }

    public Cursor countCarMyList(long listId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("COUNT(D.CAR) ");
        query.append("FROM PRODUCT_LIST LP ");
        query.append("INNER JOIN PRODUCT P ON P._id = LP.PRODUCT_ID ");
        query.append("NATURAL JOIN PRODUCT_DETAIL D ");
        query.append("WHERE LP.LIST_ID = ? AND D.CAR <> 0");
        return getReadableDatabase().rawQuery(
                query.toString(),
                new String[] {Long.toString(listId)}
        );
    }

    public void updateProductIntoCar(long listId, long productId, int car) {
        ContentValues values = new ContentValues();
        values.put("CAR", car);
        getWritableDatabase().update(
                "PRODUCT_DETAIL",
                values,
                "LIST_ID = ? AND PRODUCT_ID = ?",
                new String[] {String.valueOf(listId), String.valueOf(productId)});
    }

    public void deleteProducList(long idProduct, long listId) {
        getWritableDatabase().delete(
                "PRODUCT_LIST",
                "PRODUCT_ID = ? AND LIST_ID = ?",
                new String[] {String.valueOf(idProduct), String.valueOf(listId)});
    }

    public Cursor getCategories(Toast msgException) {
        try {
            cursor = getReadableDatabase().rawQuery(
                    "SELECT * FROM CATEGORY",
                    null);
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getProducts(long categoryId, Toast msgException) {
        try {
            cursor = getReadableDatabase().rawQuery(
                    "SELECT * FROM PRODUCT WHERE CATEGORY_ID = ?",
                    new String[] {Long.toString(categoryId)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getProduct(long id, Toast msgException) {
        try {
            cursor = getReadableDatabase().rawQuery(
                    "SELECT * FROM PRODUCT WHERE _id = ?",
                    new String[] {Long.toString(id)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getAllProducts(Toast msgException) {
        try {
            cursor = getReadableDatabase().rawQuery(
                    "SELECT * FROM PRODUCT",
                    null);
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getMyCreatedProducts(Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT P._id, P.NAME, P.IMAGE_ID, C.NAME, C._id ");
        query.append("FROM PRODUCT P ");
        query.append("INNER JOIN CATEGORY C ON C._id = P.CATEGORY_ID ");
        query.append("WHERE P.IMAGE_ID LIKE '%my_created_product%' ");
        query.append("OR P.IMAGE_ID LIKE '%2131492872%'");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    null);
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getMyCreatedProduct(long productId, Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT P._id, P.NAME, P.IMAGE_ID, C._id AS CATEGORY_ID ");
        query.append("FROM PRODUCT P ");
        query.append("INNER JOIN CATEGORY C ON C._id = P.CATEGORY_ID ");
        query.append("WHERE P._id = ?");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    new String[] {String.valueOf(productId)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public void deleteMyCreatedProduct(long idProduct) {
        getWritableDatabase().delete(
                "PRODUCT",
                "_id = ?",
                new String[] {String.valueOf(idProduct)});
    }

    public Cursor filterProducts(String key, long categoryId, Toast msgException) {
        try {
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM PRODUCT ");
            query.append("WHERE NAME LIKE '%" +key+ "%'");
            if (categoryId != 0) {
                query.append(" AND CATEGORY_ID = " +categoryId);
            }
            cursor = getReadableDatabase().rawQuery(query.toString(), null);
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getProductsList(long listId, Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("PRODUCT_ID ");
        query.append("FROM PRODUCT_LIST ");
        query.append("WHERE LIST_ID = ?");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    new String[] {String.valueOf(listId)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getLists(int isActive, boolean formatDate, Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("_id,");
        query.append("NAME,");
        if (formatDate) {
            query.append("strftime('%d/%m/%Y', date(DATE / 1000, 'unixepoch', 'localtime')) AS DATE ");
        } else {
            query.append("DATE ");
        }
        query.append("FROM LIST ");
        query.append("WHERE ACTIVE = ?");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    new String[] {String.valueOf(isActive)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getLastList(Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("MAX(_id), ");
        query.append("strftime('%d/%m/%Y', date(DATE / 1000, 'unixepoch', 'localtime')) AS DATE ");
        query.append("FROM LIST");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(), null);
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public Cursor getListName(long listId, Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("_id,");
        query.append("NAME ");
        query.append("FROM LIST ");
        query.append("WHERE _id = ?");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    new String[] {String.valueOf(listId)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public void saveList(String name, Long date) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("DATE", date);
        values.put("ACTIVE", 1);
        getWritableDatabase().insert("LIST", null, values);
    }

    public void updataeNameList(long listId, String newName) {
        ContentValues value = new ContentValues();
        value.put("NAME", newName);
        getWritableDatabase().update(
                "LIST",
                value,
                "_id = ?",
                new String[] {String.valueOf(listId)});
    }

    public void addProductList(long listId, long productId, Toast msgException) {
        ContentValues values = new ContentValues();
        values.put("LIST_ID", listId);
        values.put("PRODUCT_ID", productId);
        try {
            getWritableDatabase().insert("PRODUCT_LIST", null, values);
        } catch (SQLiteException e) {
            msgException.show();
        }
    }

    public void addProductDetail(double price, double amount, String unity,
                                 double total, long listId, long productId) {
        ContentValues values = new ContentValues();
        values.put("PRICE", price);
        values.put("AMOUNT", amount);
        values.put("UNITY", unity);
        values.put("TOTAL", total);
        values.put("CAR", 0);
        values.put("LIST_ID", listId);
        values.put("PRODUCT_ID", productId);
        getWritableDatabase().insert("PRODUCT_DETAIL", null, values);
    }

    public Cursor getProductDetail(long listId, long productId, Toast msgException) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("P._id, ");
        query.append("P.NAME, P.IMAGE_ID, P.FAVORITE, ");
        query.append("replace(printf('%.2f', D.PRICE),'.',',') AS PRICE, ");
        query.append("D.AMOUNT, D.UNITY, ");
        query.append("replace(printf('%.2f', D.TOTAL),'.',',') AS TOTAL ");
        query.append("FROM PRODUCT_LIST LP ");
        query.append("INNER JOIN PRODUCT P ON P._id = LP.PRODUCT_ID ");
        query.append("NATURAL JOIN PRODUCT_DETAIL D ");
        query.append("WHERE LP.LIST_ID = ? AND LP.PRODUCT_ID = ?");
        try {
            cursor = getReadableDatabase().rawQuery(
                    query.toString(),
                    new String[] {String.valueOf(listId), String.valueOf(productId)});
        } catch (SQLiteException e) {
            msgException.show();
        }
        return cursor;
    }

    public void updateProductDetail(double price, double amount, String unity,
                                    double total, long listId, long productId) {
        ContentValues values = new ContentValues();
        values.put("PRICE", price);
        values.put("AMOUNT", amount);
        values.put("UNITY", unity);
        values.put("TOTAL", total);
        getWritableDatabase().update(
                "PRODUCT_DETAIL",
                values,
                "LIST_ID = ? AND PRODUCT_ID = ?",
                new String[] {String.valueOf(listId), String.valueOf(productId)});
    }

    public Cursor verifyProductToList(long listId, long productId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("_id ");
        query.append("FROM PRODUCT_LIST ");
        query.append("WHERE ");
        query.append("LIST_ID = ? ");
        query.append("AND PRODUCT_ID = ? ");
        return getReadableDatabase().rawQuery(
                query.toString(),
                new String[] {String.valueOf(listId), String.valueOf(productId)});
    }

    public Cursor findProductByNameAndCategory(String productName, long categoryId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("NAME, CATEGORY_ID ");
        query.append("FROM PRODUCT ");
        query.append("WHERE ");
        query.append("NAME = ? ");
        query.append("AND CATEGORY_ID = ? ");
        return getReadableDatabase().rawQuery(
                query.toString(),
                new String[] {productName, String.valueOf(categoryId)});
    }

    public void updateActiveList(long listId, int isActive) {
        ContentValues values = new ContentValues();
        values.put("ACTIVE", isActive);
        getWritableDatabase().update(
                "LIST",
                values,
                "_id = ?",
                new String[] {String.valueOf(listId)});
    }

    public void deleteList(long id) {
        getWritableDatabase().delete(
                "LIST",
                "_id = ?",
                new String[] {String.valueOf(id)});
    }

    public Cursor getFavorites() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("_id, ");
        query.append("NAME, ");
        query.append("IMAGE_ID ");
        query.append("FROM PRODUCT ");
        query.append("WHERE FAVORITE = ?");
        return getReadableDatabase().rawQuery(
                query.toString(),
                new String[] {"1"});
    }

    public Cursor getListDrawer() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append("_id, ");
        query.append("NAME, ");
        query.append("ICON ");
        query.append("FROM LIST_DRAWER ");
        return getReadableDatabase().rawQuery(query.toString(), null);
    }

    public void updateFavorite(long productId, int isFavorite) {
        ContentValues values = new ContentValues();
        values.put("FAVORITE", isFavorite);
        getWritableDatabase().update(
                "PRODUCT",
                values,
                "_id = ?",
                new String[] {String.valueOf(productId)});
    }

    public Cursor getCountAd() {
        return getReadableDatabase().rawQuery("SELECT * FROM COUNT_AD",null);
    }

    public void updateCountAd(String column, int count) {
        ContentValues values = new ContentValues();
        values.put(column, count);
        getWritableDatabase().update(
                "COUNT_AD",
                values,
                null,
                null);
    }

    private void insertAllProducts() {
        // Mercearia
        insertProduct("Arroz branco", R.drawable.arroz_branco, 1);
        insertProduct("Arroz integral", R.drawable.arroz_integral, 1);
        insertProduct("Arroz oriental", R.drawable.arroz_oriental, 1);
        insertProduct("Arroz parbolizado", R.drawable.arroz_parbolizado, 1);
        insertProduct("Feijão carioca", R.drawable.feijao_carioca, 1);
        insertProduct("Feijão preto", R.drawable.feijao_preto, 1);
        insertProduct("Feijão jalo", R.drawable.feijao_jalo, 1);
        insertProduct("Feijão branco", R.drawable.feijao_branco, 1);
        insertProduct("Feijão bolinha", R.drawable.feijao_bolinha, 1);
        insertProduct("Feijão resinha", R.drawable.feijao_resinha, 1);
        insertProduct("Feijão fradinho", R.drawable.feijao_fradinho, 1);
        insertProduct("Áçúcar cristal", R.drawable.acucar_cristal, 1);
        insertProduct("Áçúcar natural", R.drawable.acucar_natural, 1);
        insertProduct("Áçúcar refinado", R.drawable.acucar_refinado, 1);
        insertProduct("Áçúcar cristal orgânico", R.drawable.acucar_cristal_organico, 1);
        insertProduct("Áçúcar mascavo orgânico", R.drawable.acucar_mascavo_organico, 1);
        insertProduct("Adoçante stévia", R.drawable.adocante_stevia, 1);
        insertProduct("Adoçante sacarina", R.drawable.adocante_sacarina, 1);
        insertProduct("Adoçante sucralose", R.drawable.adocante_sucralose, 1);
        insertProduct("Adoçante em pó stévia", R.drawable.adocante_em_po_stevia, 1);
        insertProduct("Adoçante em pó aspartame", R.drawable.adocante_em_po_aspartame, 1);
        insertProduct("Adoçante em pó sucralose", R.drawable.adocante_em_po_sucralose, 1);
        insertProduct("Café", R.drawable.cafe, 1);
        insertProduct("Achocolatado em pó", R.drawable.achocolatado_em_po, 1);
        insertProduct("Chá", R.drawable.cha, 1);
        insertProduct("Cápsula de café", R.drawable.capsula_de_cafe, 1);
        insertProduct("Cappuccino", R.drawable.cappuccino, 1);
        insertProduct("Café solúvel", R.drawable.cafe_soluvel, 1);
        insertProduct("Biscoitos sortidos", R.drawable.biscoitos_sortidos, 1);
        insertProduct("Biscoito rosquinhas", R.drawable.biscoito_rosquinhas, 1);
        insertProduct("Biscoito recheado", R.drawable.biscoito_recheado, 1);
        insertProduct("Biscoito maizena", R.drawable.biscoito_maizena, 1);
        insertProduct("Biscoito integral", R.drawable.biscoito_integral, 1);
        insertProduct("Biscoito água e sal", R.drawable.biscoito_agua_e_sal, 1);
        insertProduct("Biscoito", R.drawable.biscoito, 1);
        insertProduct("Batata palha", R.drawable.batata_palha, 1);
        insertProduct("Batata ondulada", R.drawable.batata_ondulada, 1);
        insertProduct("Amendoim japonês", R.drawable.amendoim_japones, 1);
        insertProduct("Granola", R.drawable.granola, 1);
        insertProduct("Farinha de trigo", R.drawable.farinha_de_trigo, 1);
        insertProduct("Farinha láctea", R.drawable.farinha_lactea, 1);
        insertProduct("Farinha de rosca", R.drawable.farinha_de_rosca, 1);
        insertProduct("Farinha de milho", R.drawable.farinha_de_milho, 1);
        insertProduct("Farinha de mandioca", R.drawable.farinha_de_mandioca, 1);
        insertProduct("Farofa", R.drawable.farofa, 1);
        insertProduct("Fubá", R.drawable.fuba, 1);
        insertProduct("Cereal matinal", R.drawable.cereal_matinal, 1);
        insertProduct("Aveia em flocos", R.drawable.aveia_em_flocos, 1);
        insertProduct("Milho para pipoca de micro-ondas", R.drawable.milho_para_pipoca_microondas, 1);
        insertProduct("Milho para pipoca", R.drawable.milho_para_pipoca, 1);
        insertProduct("Milho para canjica", R.drawable.milho_para_canjica, 1);
        insertProduct("Óleo de soja", R.drawable.oleo_de_soja, 1);
        insertProduct("Óleo composto", R.drawable.oleo_composto, 1);
        insertProduct("Óleo de milho", R.drawable.oleo_de_milho, 1);
        insertProduct("Óleo de canola", R.drawable.oleo_de_canola, 1);
        insertProduct("Óleo de girassol", R.drawable.oleo_de_girassol, 1);
        insertProduct("Macarrão pena", R.drawable.macarrao_pena, 1);
        insertProduct("Macarrão parafuso", R.drawable.macarrao_parafuso, 1);
        insertProduct("Macarrão padre nosso", R.drawable.macarrao_padre_nosso, 1);
        insertProduct("Macarrão cabelo de anjo", R.drawable.macarrao_cabelo_de_anjo, 1);
        insertProduct("Macarrão caracol", R.drawable.macarrao_caracol, 1);
        insertProduct("Macarrão espaguete", R.drawable.macarrao_espaguete, 1);
        insertProduct("Macarrão instantâneo", R.drawable.macarrao_instantaneo, 1);
        insertProduct("Nhoque", R.drawable.nhoque, 1);
        insertProduct("Massa para lasanha", R.drawable.massa_para_lasanha, 1);
        insertProduct("Ravioli", R.drawable.ravioli, 1);
        insertProduct("Lasanha", R.drawable.lasanha, 1);
        insertProduct("Extrato de tomate", R.drawable.extrato_de_tomate, 1);
        insertProduct("Creme galinha", R.drawable.creme_galinha, 1);
        insertProduct("Creme de cebola", R.drawable.creme_de_cebola, 1);
        insertProduct("Creme de queijo", R.drawable.creme_de_queijo, 1);
        insertProduct("Creme de ervilha com bacon", R.drawable.creme_de_ervilha_com_bacon, 1);
        insertProduct("Sopa de carne", R.drawable.sopa_de_carne, 1);
        insertProduct("Sopa de feijão", R.drawable.sopa_de_feijao, 1);
        insertProduct("Sopa de galinha", R.drawable.sopa_de_galinha, 1);
        insertProduct("Sopa de costela com legumes", R.drawable.sopa_de_costela_com_legumes, 1);
        insertProduct("Sopa de carne com legumes", R.drawable.sopa_de_carne_com_legumes, 1);
        insertProduct("Vitamina", R.drawable.vitamina, 1);
        insertProduct("Palmito", R.drawable.palmito, 1);
        insertProduct("Milho e ervilha", R.drawable.milho_e_ervilha, 1);
        insertProduct("Milho", R.drawable.milho, 1);
        insertProduct("Ervilha", R.drawable.ervilha, 1);
        insertProduct("Azeitonas", R.drawable.azeitonas, 1);
        insertProduct("Atum", R.drawable.atum, 1);
        insertProduct("Sardinha", R.drawable.sardinha_lata, 1);
        insertProduct("Molho para salada rosé", R.drawable.molho_para_salada_rose, 1);
        insertProduct("Molho para salada caseiro", R.drawable.molho_para_salada_caseiro, 1);
        insertProduct("Molho para salada parmesão", R.drawable.molho_para_salada_parmesao, 1);
        insertProduct("Molho para salada mostarda", R.drawable.molho_para_salada_mostarda, 1);
        insertProduct("Molho para salada limão", R.drawable.molho_para_salada_limao, 1);
        insertProduct("Molho para salada italiano", R.drawable.molho_para_salada_italiano, 1);
        insertProduct("Molho para salada caesar", R.drawable.molho_para_salada_caesar, 1);
        insertProduct("Molho inglês", R.drawable.molho_ingles, 1);
        insertProduct("Molho shoyu", R.drawable.molho_shoyu, 1);
        insertProduct("Molho de pimenta", R.drawable.molho_de_pimenta, 1);
        insertProduct("Mostarda", R.drawable.mostarda, 1);
        insertProduct("Catchup", R.drawable.catchup, 1);
        insertProduct("Maionese", R.drawable.maionese, 1);
        insertProduct("Manjericão", R.drawable.manjericao, 1);
        insertProduct("Folha de louro", R.drawable.folha_de_louro, 1);
        insertProduct("Erva doce", R.drawable.erva_doce, 1);
        insertProduct("Cravo da índia", R.drawable.cravo_da_india, 1);
        insertProduct("Colorífico", R.drawable.colorifico, 1);
        insertProduct("Coentro", R.drawable.coentro, 1);
        insertProduct("Pimenta do reino", R.drawable.pimenta_do_reino, 1);
        insertProduct("Orégano", R.drawable.oregano, 1);
        insertProduct("Cheiro verde", R.drawable.cheiro_verde, 1);
        insertProduct("Tempero baiano", R.drawable.tempero_baiano, 1);
        insertProduct("Bicabornato de sódio", R.drawable.bicabornato_de_sodio, 1);
        insertProduct("Alercrim", R.drawable.alercrim, 1);
        insertProduct("Açafrão da terra", R.drawable.acafrao_da_terra, 1);
        insertProduct("Canela da china em pó", R.drawable.canela_da_china_em_po, 1);
        insertProduct("Canela da china", R.drawable.canela_da_china, 1);
        insertProduct("Camomila", R.drawable.camomila, 1);
        insertProduct("Caldo de picanha", R.drawable.caldo_de_picanha, 1);
        insertProduct("Caldo de legumes", R.drawable.caldo_de_legumes, 1);
        insertProduct("Caldo de galinha", R.drawable.caldo_de_galinha, 1);
        insertProduct("Caldo de costela", R.drawable.caldo_de_costela, 1);
        insertProduct("Caldo de carne", R.drawable.caldo_de_carne, 1);
        insertProduct("Caldo de bacon", R.drawable.caldo_de_bacon, 1);
        insertProduct("Tempero para feijão", R.drawable.tempero_para_feijao, 1);
        insertProduct("Tempero para carnes", R.drawable.tempero_para_carnes, 1);
        insertProduct("Tempero para avez", R.drawable.tempero_para_avez, 1);
        insertProduct("Tempero para arroz", R.drawable.tempero_para_arroz, 1);
        insertProduct("Tempero de alho e sal", R.drawable.tempero_de_alho_e_sal, 1);
        insertProduct("Alho frito", R.drawable.alho_frito, 1);
        insertProduct("Alho", R.drawable.alho, 1);
        insertProduct("Sal", R.drawable.sal, 1);
        insertProduct("Azeite de oliva extra virgem", R.drawable.azeite_de_oliva_extra_virgem, 1);
        insertProduct("Vinagre balsâmico", R.drawable.vinagre_balsamico, 1);
        insertProduct("Vinagre", R.drawable.vinagre, 1);

        // MERCEARIA DOCE
        insertProduct("Pastilha", R.drawable.pastilha, 2);
        insertProduct("Chocolate", R.drawable.chocolate, 2);
        insertProduct("Chiclete", R.drawable.chiclete, 2);
        insertProduct("Bombons", R.drawable.bombons, 2);
        insertProduct("Barra de cereais", R.drawable.barra_de_cereais, 2);
        insertProduct("Bala refrescante", R.drawable.bala_refrescante, 2);
        insertProduct("Bala gelatinas", R.drawable.bala_gelatinas, 2);
        insertProduct("Bala de iogurte", R.drawable.bala_de_iogurte, 2);
        insertProduct("Geleia", R.drawable.geleia, 2);
        insertProduct("Gelatina", R.drawable.gelatina, 2);
        insertProduct("Fermento", R.drawable.fermento, 2);
        insertProduct("Chocolate em pó", R.drawable.chocolate_em_po, 2);
        insertProduct("Doce de leite", R.drawable.doce_de_leite, 2);
        insertProduct("Creme de leite", R.drawable.creme_de_leite, 2);
        insertProduct("Coco ralado", R.drawable.coco_ralado, 2);
        insertProduct("Cobertura", R.drawable.cobertura, 2);
        insertProduct("Chantilly spray", R.drawable.chantilly_spray, 2);
        insertProduct("Chantilly", R.drawable.chantilly, 2);
        insertProduct("Cereja em calda", R.drawable.cereja_em_calda, 2);
        insertProduct("Ameixa seca", R.drawable.ameixa_seca, 2);
        insertProduct("Mistura para bolo", R.drawable.mistura_para_bolo, 2);
        insertProduct("Leite de coco", R.drawable.leite_de_coco, 2);
        insertProduct("Leite condensado", R.drawable.leite_condensado, 2);
        insertProduct("Granulado", R.drawable.granulado, 2);

        // LATICINIOS
        insertProduct("Alimento de soja iogurte", R.drawable.alimento_de_soja_iogurte, 3);
        insertProduct("Bebida láctea", R.drawable.bebida_lactea, 3);
        insertProduct("Iogurte com frutas", R.drawable.iogurte_com_frutas, 3);
        insertProduct("Iogurte líquido", R.drawable.iogurte_liquido, 3);
        insertProduct("Iogurte polpa", R.drawable.iogurte_polpa, 3);
        insertProduct("Iogurte tradicional", R.drawable.iogurte_tradicional, 3);
        insertProduct("Leite fermentado", R.drawable.leite_fermentado, 3);
        insertProduct("Leite fermentado polpa", R.drawable.leite_fermentado_polpa, 3);
        insertProduct("Petit suisse", R.drawable.petit_suisse, 3);
        insertProduct("Sobremesa láctea", R.drawable.sobremesa_lactea, 3);
        insertProduct("Banha", R.drawable.banha, 3);
        insertProduct("Coalha", R.drawable.coalha, 3);
        insertProduct("Cream cheese", R.drawable.cream_cheese, 3);
        insertProduct("Gordura vegetel", R.drawable.gordura_vegetel, 3);
        insertProduct("Manteiga com sal", R.drawable.manteiga_com_sal, 3);
        insertProduct("Manteiga extra sem sal", R.drawable.manteiga_extra_sem_sal, 3);
        insertProduct("Margarina com sal", R.drawable.margarina_com_sal, 3);
        insertProduct("Margarina sem sal", R.drawable.margarina_sem_sal, 3);
        insertProduct("Margarina vegetal", R.drawable.margarina_vegetal, 3);
        insertProduct("Margarina vegetal com sal", R.drawable.margarina_vegetal_com_sal, 3);
        insertProduct("Nata", R.drawable.nata, 3);
        insertProduct("Requeijão", R.drawable.requeijao, 3);
        insertProduct("Leite desnatado", R.drawable.leite_desnatado, 3);
        insertProduct("Leite em pó", R.drawable.leite_em_po, 3);
        insertProduct("Leite integral", R.drawable.leite_integral, 3);
        insertProduct("Leite semidesnatado", R.drawable.leite_semi_desnatado, 3);
        insertProduct("Leite zero lactose", R.drawable.leite_zero_lactose, 3);
        insertProduct("Queijo gorgonzola", R.drawable.queijo_gorgonzola, 3);
        insertProduct("Queijo minas", R.drawable.queijo_minas, 3);
        insertProduct("Queijo mussarela fatiado", R.drawable.queijo_mussarela_fatiado, 3);
        insertProduct("Queijo mussarela pedaço", R.drawable.queijo_mussarela_pedaco, 3);
        insertProduct("Queijo parmesão pedaço", R.drawable.queijo_parmesao_pedaco, 3);
        insertProduct("Queijo parmesão ralado", R.drawable.queijo_parmesao_ralado, 3);
        insertProduct("Queijo prato fatiado", R.drawable.queijo_prato_fatiado, 3);
        insertProduct("Queijo prato pedaço", R.drawable.queijo_prato_pedaco, 3);
        insertProduct("Queijo processado", R.drawable.queijo_processado, 3);
        insertProduct("Queijo provolone pedaço", R.drawable.queijo_provolenw_pedaco, 3);

        // Congelados e resfriados
        insertProduct("Presunto", R.drawable.presunto, 4);
        insertProduct("Apresuntado", R.drawable.apresuntado, 4);
        insertProduct("Mortadela", R.drawable.mortadela, 4);
        insertProduct("Salame", R.drawable.salame, 4);
        insertProduct("Copa", R.drawable.copa, 4);
        insertProduct("Lombo", R.drawable.lombo, 4);
        insertProduct("Contra filé", R.drawable.contra_file, 4);
        insertProduct("Picanha", R.drawable.picanha, 4);
        insertProduct("Filé mignon", R.drawable.file_mignon, 4);
        insertProduct("Maminha", R.drawable.maminha, 4);
        insertProduct("Alcatra", R.drawable.alcatra, 4);
        insertProduct("Alcatra com maminha", R.drawable.alcatra_com_maminha, 4);
        insertProduct("Coxão mole", R.drawable.coxao_mole, 4);
        insertProduct("Coxão duro", R.drawable.coxao_duro, 4);
        insertProduct("Patinho", R.drawable.patinho, 4);
        insertProduct("Acém", R.drawable.acem, 4);
        insertProduct("Carne moida", R.drawable.carne_moida, 4);
        insertProduct("Paleta", R.drawable.paleta, 4);
        insertProduct("Lagarto", R.drawable.lagarto, 4);
        insertProduct("Peixinho", R.drawable.peixinho, 4);
        insertProduct("Jerked beef", R.drawable.jerked_beef, 4);
        insertProduct("Fraldinha", R.drawable.fraldinha, 4);
        insertProduct("Figado", R.drawable.figado, 4);
        insertProduct("Cupim", R.drawable.cupim, 4);
        insertProduct("Filé de peito", R.drawable.file_de_peito, 4);
        insertProduct("Coxinha da asa", R.drawable.coxinha_da_asa, 4);
        insertProduct("Coxa com sobrecoxa", R.drawable.coxa_com_sobrecoxa, 4);
        insertProduct("Coxa", R.drawable.coxa, 4);
        insertProduct("Coração", R.drawable.coracao, 4);
        insertProduct("Chicken", R.drawable.chicken, 4);
        insertProduct("Sobrecoxa", R.drawable.sobrecoxa, 4);
        insertProduct("Peito sem osso", R.drawable.peito_sem_osso, 4);
        insertProduct("Moela", R.drawable.moela, 4);
        insertProduct("Meio da asa", R.drawable.meio_da_asa, 4);
        insertProduct("Frango congelado", R.drawable.frango_congelado, 4);
        insertProduct("Frango à passarinho", R.drawable.frango_a_passarinho, 4);
        insertProduct("Filé de tilápia", R.drawable.file_de_tilapia, 4);
        insertProduct("Filé de polaca", R.drawable.file_de_polaca, 4);
        insertProduct("Filé de merluza", R.drawable.file_de_merluza, 4);
        insertProduct("Filé de linguado", R.drawable.file_de_linguado, 4);
        insertProduct("Filé de cação", R.drawable.file_de_cacao, 4);
        insertProduct("Filé de pescada", R.drawable.file_de_pescada, 4);
        insertProduct("Camarão", R.drawable.camarao, 4);
        insertProduct("Bacalhau", R.drawable.bacalhau, 4);
        insertProduct("Sardinha", R.drawable.sardinha, 4);
        insertProduct("Salmão", R.drawable.salmao, 4);

        // Bebidas
        insertProduct("Água", R.drawable.agua, 5);
        insertProduct("Suco integral", R.drawable.suco_integral, 5);
        insertProduct("Suco concentrado", R.drawable.suco_concentrado, 5);
        insertProduct("Néctar garrafa", R.drawable.nectar_garrafa, 5);
        insertProduct("Néctar", R.drawable.nectar, 5);
        insertProduct("Bebida em pó", R.drawable.bebida_em_po, 5);
        insertProduct("Álimento de soja", R.drawable.alimento_de_soja, 5);
        insertProduct("Água de coco", R.drawable.agua_de_coco, 5);
        insertProduct("Chá gelado", R.drawable.cha_gelado, 5);
        insertProduct("Refrigerante de cola", R.drawable.refrigerante_de_cola, 5);
        insertProduct("Refrigerante de citrus", R.drawable.refrigerante_de_citrus, 5);
        insertProduct("Refrigerente de limão", R.drawable.refrigerente_de_limao, 5);
        insertProduct("Refrigerante de uva lata", R.drawable.refrigerante_de_uva_lata, 5);
        insertProduct("Refrigerante de uva", R.drawable.refrigerante_de_uva, 5);
        insertProduct("Refrigerante de limão lata", R.drawable.refrigerante_de_limao_lata, 5);
        insertProduct("Refrigerante de laranja lata", R.drawable.refrigerante_de_laranja_lata, 5);
        insertProduct("Refrigerante de laranja", R.drawable.refrigerante_de_laranja, 5);
        insertProduct("Refrigerante de guaraná lata", R.drawable.refrigerante_de_guarana_lata, 5);
        insertProduct("Refrigerante de guaraná", R.drawable.refrigerante_de_guarana, 5);
        insertProduct("Refrigerante de cola sem açúcar", R.drawable.refrigerante_de_cola_sem_acucar, 5);
        insertProduct("Refrigerante de cola lata sem açúcar", R.drawable.refrigerante_de_cola_lata_sem_acucar, 5);
        insertProduct("Refrigerante de cola lata", R.drawable.refrigerante_de_cola_lata, 5);
        insertProduct("Refrigerante de cola com café lata", R.drawable.refrigerante_de_cola_com_cafe_lata, 5);

        // Bebidas Alcoolicas
        insertProduct("Vodka", R.drawable.vodka, 6);
        insertProduct("Vinho tinto", R.drawable.vinho_tinto, 6);
        insertProduct("Vinho espumante", R.drawable.vinho_espumante, 6);
        insertProduct("Vinho branco", R.drawable.vinho_branco, 6);
        insertProduct("Tequila", R.drawable.tequila, 6);
        insertProduct("Sake", R.drawable.sake, 6);
        insertProduct("Licor", R.drawable.licor, 6);
        insertProduct("Conhaque", R.drawable.conhaque, 6);
        insertProduct("Champangne", R.drawable.champangne, 6);
        insertProduct("Cerveja lata", R.drawable.cerveja_lata, 6);
        insertProduct("Cerveja garrafa", R.drawable.cerveja_garrafa, 6);
        insertProduct("Cerveja escura", R.drawable.cerveja_escura, 6);
        insertProduct("Cachaça", R.drawable.cachaca, 6);
        insertProduct("Aguardente", R.drawable.aguardente, 6);
        insertProduct("Whiskey", R.drawable.whiskey, 6);

        // Higiene, saude e beleza
        insertProduct("Shampoo", R.drawable.shampoo, 7);
        insertProduct("Condicionador", R.drawable.condicionador, 7);
        insertProduct("Creme de tratamento", R.drawable.creme_de_tratamento, 7);
        insertProduct("Creme para pentear", R.drawable.creme_para_pentear, 7);
        insertProduct("Coloração", R.drawable.coloracao, 7);
        insertProduct("Escova de cabelo", R.drawable.escova_de_cabelo, 7);
        insertProduct("Gel", R.drawable.gel, 7);
        insertProduct("Reparo de pontas", R.drawable.reparo_de_pontas, 7);
        insertProduct("Touca para banho", R.drawable.touca_para_banho, 7);
        insertProduct("Bloqueador solar", R.drawable.bloqueador_solar, 7);
        insertProduct("Creme corporal", R.drawable.creme_corporal, 7);
        insertProduct("Esponja de banho", R.drawable.esponja_de_banho, 7);
        insertProduct("Hidratante", R.drawable.hidratante, 7);
        insertProduct("Leite de colonia", R.drawable.leite_de_colonia, 7);
        insertProduct("Loção hidratante", R.drawable.locao_hidratante, 7);
        insertProduct("Protetor diário", R.drawable.protetor_diario, 7);
        insertProduct("Sabonete íntimo", R.drawable.sabonete_intimo, 7);
        insertProduct("Absorvente interno", R.drawable.absorvente_interno, 7);
        insertProduct("Absorvente noturno", R.drawable.absorvente_noturno, 7);
        insertProduct("Absorvente com abas", R.drawable.absorvente_com_abas, 7);
        insertProduct("Absorvente sem abas", R.drawable.absorvente_sem_abas, 7);
        insertProduct("Aparelho de barbear", R.drawable.aparelho_de_barbear, 7);
        insertProduct("Carga para barbear", R.drawable.carga_para_barbear, 7);
        insertProduct("Espuma de barbear", R.drawable.espuma_de_barbear, 7);
        insertProduct("Loção pós barba", R.drawable.locao_pos_barba, 7);
        insertProduct("Protetor solar", R.drawable.protetor_solar, 7);
        insertProduct("Repelente", R.drawable.repelente, 7);
        insertProduct("Álcool em gel", R.drawable.alcool_em_gel, 7);
        insertProduct("Algodão", R.drawable.algodao, 7);
        insertProduct("Anti séptico", R.drawable.anti_septico, 7);
        insertProduct("Creme dental", R.drawable.creme_dental, 7);
        insertProduct("Desodorante aerosol feminino", R.drawable.desodorante_aerosol_feminino, 7);
        insertProduct("Desodorante aerosol masculino", R.drawable.desodorante_aerosol_masculino, 7);
        insertProduct("Desodorante para os pés feminino creme", R.drawable.desodorante_para_os_pes_feminino_creme, 7);
        insertProduct("Desodorante para os pés feminino pó", R.drawable.desodorante_para_os_pes_feminino_po, 7);
        insertProduct("Desodorante para os pés masculino pó", R.drawable.desodorante_para_os_pes_masculino_po, 7);
        insertProduct("Desodorante para os pés masculino spray.jpg", R.drawable.desodorante_para_os_pes_masculino_spray, 7);
        insertProduct("Desodorante roll on feminino", R.drawable.desodorante_roll_on_feminino, 7);
        insertProduct("Desodorante roll on masculino", R.drawable.desodorante_roll_on_masculino, 7);
        insertProduct("Escova dental", R.drawable.escova_dental, 7);
        insertProduct("Fio dental", R.drawable.fio_dental, 7);
        insertProduct("Fraldas", R.drawable.fraldas, 7);
        insertProduct("Lenço em papel", R.drawable.lenco_em_papel, 7);
        insertProduct("Lenço umidecido", R.drawable.lenco_umidecido, 7);
        insertProduct("Lenço umidecido para bebê", R.drawable.lenco_umidecido_para_bebe, 7);
        insertProduct("Papel higiênico", R.drawable.papel_higienico, 7);
        insertProduct("Sabonete antibacteriano", R.drawable.sabonete_antibacteriano, 7);
        insertProduct("Sabonete em barra", R.drawable.sabonete_em_barra, 7);
        insertProduct("Sabonete infantil", R.drawable.sabonete_infantil, 7);
        insertProduct("Sabonete líquido", R.drawable.sabonete_liquido, 7);
        insertProduct("Shampoo para criança", R.drawable.shampoo_para_crianca, 7);
        insertProduct("Condicionador para bebê", R.drawable.condicionador_para_bebe, 7);
        insertProduct("Shampoo para bebe", R.drawable.shampoo_para_bebe, 7);
        insertProduct("Talco para bebê", R.drawable.talco_para_bebe, 7);
        insertProduct("Creme para assaduras", R.drawable.creme_para_assaduras, 7);
        insertProduct("Curativos", R.drawable.curativos, 7);

        // Limpeza
        insertProduct("Álcool", R.drawable.alcool, 8);
        insertProduct("Candida", R.drawable.candida, 8);
        insertProduct("Cera", R.drawable.cera, 8);
        insertProduct("Limpador", R.drawable.limpador, 8);
        insertProduct("Amaciante", R.drawable.amaciante, 8);
        insertProduct("Detergente em pó", R.drawable.detergente_em_po, 8);
        insertProduct("Naftalina", R.drawable.naftalina, 8);
        insertProduct("Removedor", R.drawable.removedor, 8);
        insertProduct("Evita mofo", R.drawable.evita_mofo, 8);
        insertProduct("Inseticida", R.drawable.inseticida, 8);
        insertProduct("Limpa inox", R.drawable.limpa_inox, 8);
        insertProduct("Limpa piso", R.drawable.limpa_piso, 8);
        insertProduct("Odorizador", R.drawable.odorizador, 8);
        insertProduct("Odorizante", R.drawable.odorizante, 8);
        insertProduct("Aromatizante", R.drawable.aromatizante, 8);
        insertProduct("Desinfetante", R.drawable.desinfetante, 8);
        insertProduct("Desodorizador", R.drawable.desodorizador, 8);
        insertProduct("Lustra móveis", R.drawable.lustra_moveis, 8);
        insertProduct("Água sanitária", R.drawable.agua_sanitaria, 8);
        insertProduct("Limpa alumínio", R.drawable.limpa_aluminio, 8);
        insertProduct("Sabão em pedra", R.drawable.sabao_em_pedra, 8);
        insertProduct("Desengordurante", R.drawable.desengordurante, 8);
        insertProduct("Detergente líquido", R.drawable.detergente_liquido, 8);
        insertProduct("Detergente sanitário", R.drawable.detergente_sanitario, 8);

        // HortiFruti
        insertProduct("Maçã fuji", R.drawable.maca_fuji, 9);
        insertProduct("Maçã gala", R.drawable.maca_gala, 9);
        insertProduct("Maçã verde", R.drawable.maca_verde, 9);
        insertProduct("Laranja lima", R.drawable.laranja_lima, 9);
        insertProduct("Laranja pêra", R.drawable.laranja_pera, 9);
        insertProduct("Laranja pêra orgânica", R.drawable.laranja_pera_organica, 9);
        insertProduct("Uva vermelha", R.drawable.uva_vermelha, 9);
        insertProduct("Uva verde", R.drawable.uva_verde, 9);
        insertProduct("Manga tommy", R.drawable.manga_tommy, 9);
        insertProduct("Manga palmer", R.drawable.manga_palmer, 9);
        insertProduct("Banana prata", R.drawable.banana_prata, 9);
        insertProduct("Banana nanica", R.drawable.banana_nanica, 9);
        insertProduct("Mamão papaya", R.drawable.mamao_papaya, 9);
        insertProduct("Morango", R.drawable.morango, 9);
        insertProduct("Pêra", R.drawable.pera, 9);
        insertProduct("Melão", R.drawable.melao, 9);
        insertProduct("Melancia", R.drawable.melancia, 9);
        insertProduct("Maracujá", R.drawable.maracuja, 9);
        insertProduct("Abacaxi", R.drawable.abacaxi, 9);
        insertProduct("Abacate", R.drawable.abacate, 9);
        insertProduct("Limão", R.drawable.limao, 9);
        insertProduct("Kiwi", R.drawable.kiwi, 9);
        insertProduct("Coco", R.drawable.coco, 9);
        insertProduct("Damasco", R.drawable.damasco, 9);
        insertProduct("Figo", R.drawable.figo, 9);
        insertProduct("Framboesa", R.drawable.framboesa, 9);
        insertProduct("Mirtilo", R.drawable.mirtilo, 9);
        insertProduct("Tomate", R.drawable.tomate, 9);
        insertProduct("Rabanete", R.drawable.rabanete, 9);
        insertProduct("Quiabo", R.drawable.quiabo, 9);
        insertProduct("Pepino japonês", R.drawable.pepino_japones, 9);
        insertProduct("Pepino comum", R.drawable.pepino_comum, 9);
        insertProduct("Abobrinha", R.drawable.abobrinha, 9);
        insertProduct("Abobora", R.drawable.abobora, 9);
        insertProduct("Vagem", R.drawable.vagem, 9);
        insertProduct("Milho verde", R.drawable.milho_verde, 9);
        insertProduct("Mandioquinha", R.drawable.mandioquinha, 9);
        insertProduct("Batata lavada", R.drawable.batata_lavada, 9);
        insertProduct("Batata monalisa", R.drawable.batata_monalisa, 9);
        insertProduct("Batata doce", R.drawable.batata_doce, 9);
        insertProduct("Berinjela", R.drawable.berinjela, 9);
        insertProduct("Beterraba", R.drawable.beterraba, 9);
        insertProduct("Cenoura", R.drawable.cenoura, 9);
        insertProduct("Chuchu", R.drawable.chuchu, 9);
        insertProduct("Ervilha", R.drawable.ervilha_horti, 9);
        insertProduct("Inhame", R.drawable.inhame, 9);
        insertProduct("Mandioca", R.drawable.mandioca, 9);
        insertProduct("Ovos vermelhos", R.drawable.ovos_vermelhos, 9);
        insertProduct("Ovos brancos", R.drawable.ovos_brancos, 9);
        insertProduct("Ovos de codorna", R.drawable.ovos_de_codorna, 9);
        insertProduct("Salsa cebolinha", R.drawable.salsa_cebolinha, 9);
        insertProduct("Pimentão", R.drawable.pimentao, 9);
        insertProduct("Mostarda", R.drawable.mostarda_horti, 9);
        insertProduct("Manjerona", R.drawable.manjerona, 9);
        insertProduct("Manjericão", R.drawable.manjericao_horti, 9);
        insertProduct("Hortelã", R.drawable.hortela, 9);
        insertProduct("Gengibre", R.drawable.gengibre, 9);
        insertProduct("Folha de louro", R.drawable.folha_de_louro_horti, 9);
        insertProduct("Cheiro verde", R.drawable.cheiro_verde_horti, 9);
        insertProduct("Cebolinha", R.drawable.cebolinha, 9);
        insertProduct("Cebola", R.drawable.cebola, 9);
        insertProduct("Alecrim", R.drawable.alecrim, 9);
        insertProduct("Salsão", R.drawable.salsao, 9);
        insertProduct("Alface roxa", R.drawable.alface_roxa, 9);
        insertProduct("Alface lisa", R.drawable.alface_lisa, 9);
        insertProduct("Alface crespa", R.drawable.alface_crespa, 9);
        insertProduct("Alface americana", R.drawable.alface_americana, 9);
        insertProduct("Alface bicolor", R.drawable.alface_bicolor, 9);
        insertProduct("Acelga", R.drawable.acelga, 9);
        insertProduct("Repolho", R.drawable.repolho, 9);
        insertProduct("Almeirão", R.drawable.almeirao, 9);
        insertProduct("Brocolis", R.drawable.brocolis, 9);
        insertProduct("Catalonia", R.drawable.catalonia, 9);
        insertProduct("Couve flor", R.drawable.couve_flor, 9);
        insertProduct("Escarola", R.drawable.escarola, 9);
        insertProduct("Espinafre", R.drawable.espinafre, 9);

        // Novos Produtos
        insertProduct("Bacon", R.drawable.bacon, 4);
        insertProduct("Linguiça calabresa", R.drawable.linguica_calabresa, 4);
        insertProduct("Linguiça de frango", R.drawable.linguica_de_frango, 4);
        insertProduct("Linguiça_toscana", R.drawable.linguica_toscana, 4);
        insertProduct("Costela", R.drawable.costela, 4);
        insertProduct("Cotonete", R.drawable.cotonete, 7);
        insertProduct("Esponja de aço", R.drawable.esponja_aco, 8);
    }
}
package br.com.up.list.utils;

import java.util.HashMap;
import java.util.Map;

public class Settings {

    private static Settings instance;
    private Map<Long, Integer> orderList = new HashMap<>();

    private Settings(){};

    public static synchronized Settings getInstance() {
        if (instance == null) {
            return instance = new Settings();
        } else {
            return instance;
        }
    }

    public void setOrderProduct(Long listId, int type) {
        if (orderList.get(listId) != null) {
            orderList.remove(listId);
            orderList.put(listId, type);
        } else {
            orderList.put(listId, type);
        }
    }

    public int getOrderProduct(Long listId) {
        if (orderList.get(listId) != null) {
            return orderList.get(listId).intValue();
        } else {
            return 1;
        }
    }

}

package br.com.up.list.utils;

import android.database.Cursor;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.Observer;
import br.com.up.list.adpter.ProductCheckAdapter;
import br.com.up.list.database.SQLiteHelper;

public final class Search {

    private Search() {}

    private static void createTrasition(ViewGroup viewGroup) {
        Transition transition;
        transition = new Fade();
        transition.setDuration(200);
        TransitionManager.beginDelayedTransition(viewGroup, transition);
    }

    public static void open(ViewGroup viewGroup, View view) {
        createTrasition(viewGroup);
        view.setVisibility(View.VISIBLE);
    }

    public static View.OnClickListener close(final ViewGroup viewGroup, final View view) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTrasition(viewGroup);
                view.setVisibility(View.INVISIBLE);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                view.setVisibility(View.GONE);
                            }
                        },
                        240
                );
            }
        };
    }

    public static TextWatcher filter(final SQLiteHelper sqLiteHelper,
                                     final long categoryId,
                                     final Toast msgException,
                                     final ListView listView,
                                     final Observer observer) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence key, int start, int before, int count) {
                 Cursor cursorProducts = sqLiteHelper.filterProducts(key.toString(), categoryId, msgException);
                listView.setAdapter(null);
                ProductCheckAdapter productCheckAdapter = new ProductCheckAdapter(listView.getContext(), cursorProducts, 0);
                listView.setAdapter(productCheckAdapter);
                productCheckAdapter.getObservable().addObserver(observer);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }
}

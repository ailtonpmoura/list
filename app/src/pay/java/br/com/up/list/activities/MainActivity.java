package br.com.up.list.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import br.com.up.list.R;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.fragments.AllProductsFragment;
import br.com.up.list.fragments.CategoryFragment;
import br.com.up.list.fragments.CreateProductFragment;
import br.com.up.list.fragments.FavoriteFragment;
import br.com.up.list.fragments.ListDeletedFragment;
import br.com.up.list.fragments.ListFragment;
import br.com.up.list.fragments.MyCreatedProductsFragment;

public class MainActivity extends Activity {

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBar actionBar;
    private int currentPosition = 0;
    private SQLiteHelper sqLiteHelper;

    private void setActionBarTitle(int position) {
        String[] titles = getResources().getStringArray(R.array.titles);
        actionBar.setTitle(titles[position]);
    }

    private void fragmentTransaction(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, "visible_fragment");
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    private void selectFragment(int position) {
        switch (position) {
            case 0:
                fragmentTransaction(new ListFragment());
                break;
            case 1:
                fragmentTransaction(new CategoryFragment());
                break;
            case 2:
                fragmentTransaction(new FavoriteFragment());
                break;
            case 3:
                fragmentTransaction(new AllProductsFragment());
                break;
            case 4:
                fragmentTransaction(new ListDeletedFragment());
                break;
            case 5:
                fragmentTransaction(new CreateProductFragment());
                break;
            case 6:
                fragmentTransaction(new MyCreatedProductsFragment());
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private void insertData(Intent intent) {
        Toast msgException = Toast.makeText(this,
        getResources().getString(R.string.exception_search), Toast.LENGTH_SHORT);

        try {
            int type = intent.getIntExtra("type", 0);

            Cursor cursorBackup = sqLiteHelper.getBackup(msgException);

            if (cursorBackup.moveToFirst()) {
                if(cursorBackup.getLong(0) == 1) {
                    backToSaveData(type,1);
                    return;
                }
            }

            if (type == 0) {
                JSONArray jsonListCategories = new JSONArray(intent.getStringExtra(Intent.EXTRA_TEXT));

                for (int i = 0; i < jsonListCategories.length(); i++) {
                    String categoryName = jsonListCategories.getString(i);
                    sqLiteHelper.insertNewCategory(categoryName);
                }
            }

            if (type == 1) {
                /*
                 * Insere as listas criadas e seus produtos
                 */
                JSONArray jsonListProduct = new JSONArray(intent.getStringExtra(Intent.EXTRA_TEXT));

                for (int i = 0; i < jsonListProduct.length(); i++) {
                    JSONArray listMap = jsonListProduct.getJSONArray(i);
                    long listId = 0;

                    for (int j = 0; j < listMap.length(); j++) {
                        JSONObject map = listMap.getJSONObject(j);
                        Iterator<String> itKeyMap = map.keys();

                        while (itKeyMap.hasNext()) {
                            if (j == 0) {
                                String listKey = itKeyMap.next();
                                JSONArray listDetail = (JSONArray) map.get(listKey);
                                sqLiteHelper.saveList(listDetail.getString(0), Long.parseLong(listDetail.getString(1)));
                                Cursor lastListCursor = sqLiteHelper.getLastList(msgException);
                                if (lastListCursor.moveToFirst()) {
                                    listId = lastListCursor.getLong(0);
                                }
                            }
                            if (j == 1) {
                                String productId = itKeyMap.next();
                                JSONArray productDetail = (JSONArray) map.get(productId);

                                if (productDetail.getString(5).contains("_my_created_product_")) {
                                    Cursor cursorProduct = sqLiteHelper.findProductByNameAndCategory(
                                            productDetail.getString(4), productDetail.getLong(6));
                                    if (cursorProduct.moveToFirst()) {
                                        productId = String.valueOf(cursorProduct.getLong(0));
                                    }
                                }

                                sqLiteHelper.addProductList(listId, Long.parseLong(productId), msgException);

                                sqLiteHelper.addProductDetail(
                                        Double.parseDouble(productDetail.getString(0).replace(",",".")),
                                        productDetail.getDouble(1),
                                        productDetail.getString(2),
                                        Double.parseDouble(productDetail.getString(3).replace(",",".")),
                                        listId, Long.parseLong(productId), msgException
                                );
                            }
                        }
                    }
                }
            } else if(type == 2) {
                /*
                 * Insere os produtos que foram criados
                 */
                JSONArray jsonCreatedProduct = new JSONArray(intent.getStringExtra(Intent.EXTRA_TEXT));

                String productName = jsonCreatedProduct.getString(0);
                String pathImage = jsonCreatedProduct.getString(1);
                Long categoryId = jsonCreatedProduct.getLong(2);
                JSONArray bytesImage = jsonCreatedProduct.getJSONArray(3);

                int indexFirtsNameImage = pathImage.lastIndexOf("/") + 1;
                String nameImage = pathImage.substring(indexFirtsNameImage, pathImage.length() - 4);

                byte[] bytes = new byte[bytesImage.length()];

                for (int i = 0; i < bytesImage.length(); i++) {
                    bytes[i] = ((Integer) bytesImage.get(i)).byteValue();
                }

                File path = createDirectoryAndSaveImageStream(bytes, nameImage);

                sqLiteHelper.insertNewProduct(productName, path.getAbsolutePath(), categoryId);
            }

            backToSaveData(type, 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void backToSaveData(int type, int backup) {
        if (type == 1 && backup == 0) {
            sqLiteHelper.updateBackup(1);
            Toast.makeText(this,
                    getResources().getString(R.string.data_send_sucefull), Toast.LENGTH_SHORT).show();
        }
        if (type == 1 && backup == 1) {
            Toast.makeText(this,
                    getResources().getString(R.string.data_sended), Toast.LENGTH_SHORT).show();
        }

        super.onBackPressed();
    }

    // Cria um novo diretório e salva a imagem
    private File createDirectoryAndSaveImageStream(byte[] bytes, String name) {
        File imagePath = null;
        try {
            File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            imagePath = File.createTempFile(
                    name,
                    ".png",
                    storageDir
            );
            FileOutputStream fileOutputStream = new FileOutputStream(imagePath);
            fileOutputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imagePath;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sqLiteHelper = new SQLiteHelper(this);

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList = findViewById(R.id.drawer_list);
        actionBar = getActionBar();

        Intent intent = getIntent();

        boolean initByFragmentCreatedProduct = intent.getBooleanExtra("productCreateOrUpdate", false);
        boolean initByMyCreatedProductsFragment = intent.getBooleanExtra("productEdit", false);

        // Init By List Free
        String action = intent.getAction();
        String type = intent.getType();

        if (action != null && action.equals(Intent.ACTION_INSERT) && type != null) {
            if (type.equals("text/plain")) {
                insertData(intent);
            }
        }

        if(savedInstanceState != null) {
            setActionBarTitle(savedInstanceState.getInt("position"));
        } else {
            if(initByFragmentCreatedProduct) {
                selectFragment(6);
                setActionBarTitle(6);
            } else if (initByMyCreatedProductsFragment){
                long productId = getIntent().getLongExtra("productId", 0);
                CreateProductFragment createProductFragment = new CreateProductFragment();
                createProductFragment.setProductId(productId);
                fragmentTransaction(createProductFragment);
                setActionBarTitle(7);
            } else {
                selectFragment(0);
            }
        }

        drawerList.setAdapter(new SimpleCursorAdapter(
                getApplicationContext(),
                R.layout.item_custom_drawer,
                sqLiteHelper.getListDrawer(),
                new String[] {"NAME","ICON"},
                new int[] {R.id.drawer_item_name, R.id.drawer_item_icon},
                0
        ));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectFragment(position);
                setActionBarTitle(position);
                drawerLayout.closeDrawer(findViewById(R.id.drawer_list));
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.open_drawer, R.string.close_drawer) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }
            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        drawerToggle.syncState();

        getFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        Fragment fragment = getFragmentManager().findFragmentByTag("visible_fragment");
                        if(fragment instanceof ListFragment) {
                            currentPosition = 0;
                        }
                        if(fragment instanceof CategoryFragment) {
                            currentPosition = 1;
                        }
                        if(fragment instanceof FavoriteFragment) {
                            currentPosition = 2;
                        }
                        if(fragment instanceof AllProductsFragment) {
                            currentPosition = 3;
                        }
                        if(fragment instanceof ListDeletedFragment) {
                            currentPosition = 4;
                        }
                        if(fragment instanceof CreateProductFragment) {
                            long productId = getIntent().getLongExtra("productId", 0);
                            if (productId != 0) {
                                currentPosition = 7;
                            } else {
                                currentPosition = 5;
                            }
                        }
                        if(fragment instanceof MyCreatedProductsFragment) {
                            currentPosition = 6;
                        }
                        setActionBarTitle(currentPosition);
                        drawerList.setItemChecked(currentPosition, true);
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt("position", currentPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}

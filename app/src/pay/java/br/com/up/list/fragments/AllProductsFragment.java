package br.com.up.list.fragments;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import br.com.up.list.R;
import br.com.up.list.adpter.ProductCheckAdapter;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.dialog.DialogAddProductsChecked;
import br.com.up.list.entity.ProductCheck;
import br.com.up.list.utils.ProductClickListener;
import br.com.up.list.utils.Search;

public class AllProductsFragment extends Fragment implements Observer {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private View fragmentView;
    private View layoutSearch;
    private ViewGroup viewGroup;
    private ImageView iconCloseSearch;
    private EditText edtSearch;
    private Button btnAddCheckedProducts;
    private boolean btnAddCheckedProductsVisible;
    private ListView listAllProducts;
    private ProductCheckAdapter productCheckAdapter;
    private static List<Long> productsIdsCheckedList = new ArrayList<>();
    private ProductClickListener productClickListener = new ProductClickListener();

    public AllProductsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_all_products,
                container,
                false);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);

        iconCloseSearch = fragmentView.findViewById(R.id.icon_close_search);
        edtSearch = fragmentView.findViewById(R.id.edt_search);
        layoutSearch = fragmentView.findViewById(R.id.layout_search);
        viewGroup = fragmentView.findViewById(R.id.layout_frag_all_products);
        listAllProducts = fragmentView.findViewById(R.id.list_all_products);
        btnAddCheckedProducts = fragmentView.findViewById(R.id.btn_add_checked_products);

        Toast msgException = Toast.makeText(
                context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        Cursor cursorAllProducts = sqLiteHelper.getAllProducts(msgException);

        ProductCheckAdapter.setCheckVisibility(View.INVISIBLE);
        ProductCheckAdapter.setProductCheckedList(new ArrayList<>());
        productCheckAdapter = new ProductCheckAdapter(context, cursorAllProducts, 0);

        listAllProducts.setAdapter(productCheckAdapter);
        productClickListener.setOnItemClick(context, listAllProducts, 0, null);
        productCheckAdapter.getObservable().addObserver(this);

        edtSearch.addTextChangedListener(Search.filter(sqLiteHelper, 0, msgException, listAllProducts, this));
        iconCloseSearch.setOnClickListener(Search.close(viewGroup, layoutSearch));

        btnAddCheckedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogAddProductsChecked(
                        context, sqLiteHelper, LayoutInflater.from(context), getResources(),
                        productsIdsCheckedList, msgException
                ).onCreateDialogList().show();
            }
        });
    }

    // Abilita/Desabilita botão para adicionar os produtos com check
    private void changeBtnAddCheckedProducts() {
        if (!btnAddCheckedProductsVisible) {
            btnAddCheckedProductsVisible = true;
            btnAddCheckedProducts.setVisibility(View.VISIBLE);
        } else {
            btnAddCheckedProductsVisible = false;
            btnAddCheckedProducts.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.products_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_search:
                Search.open(viewGroup, layoutSearch);
                break;
            case R.id.item_select:
                ProductCheckAdapter.changeCheckVisible();
                productClickListener.setOnItemClick(context, listAllProducts, 0, null);
                changeBtnAddCheckedProducts();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable o, Object productsIdsCheckedList) {
        this.productsIdsCheckedList = (List<Long>) productsIdsCheckedList;
        int countProductsChecked = this.productsIdsCheckedList.size();

        StringBuilder countProduct = new StringBuilder();
        countProduct.append(getResources().getString(R.string.btn_products_add_checked));
        countProduct.append(" (");
        countProduct.append(countProductsChecked);
        countProduct.append(")");
        btnAddCheckedProducts.setText(countProduct.toString());
    }
}

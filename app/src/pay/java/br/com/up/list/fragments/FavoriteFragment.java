package br.com.up.list.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import br.com.up.list.activities.ProductDetail;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;


public class FavoriteFragment extends Fragment {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private View fragmentView;

    public FavoriteFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_favorite,
                container,
                false
        );
    }

    @Override
    public void onStart() {
        super.onStart();

        ListView listFavorites = fragmentView.findViewById(R.id.list_favorites);
        TextView txtFavorites = fragmentView.findViewById(R.id.favorites_empty);

        Cursor cursor = sqLiteHelper.getFavorites();

        if(cursor.moveToFirst()) {
            txtFavorites.setVisibility(View.GONE);

            listFavorites.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_product,
                    cursor,
                    new String[] {"NAME", "IMAGE_ID"},
                    new int[] {R.id.product_name, R.id.product_image},
                    0
            ));

            listFavorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtProductName = view.findViewById(R.id.product_name);
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("productId", id);
                    intent.putExtra("productName", txtProductName.getText().toString());
                    startActivity(intent);
                }
            });
        } else {
            txtFavorites.setVisibility(View.VISIBLE);
        }
    }

}

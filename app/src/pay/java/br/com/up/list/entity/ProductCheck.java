package br.com.up.list.entity;

public class ProductCheck {

    private long id;
    private String productName;
    private  String productImage;
    private String categoryName;
    private boolean checked;

    public ProductCheck(long id, String productName,
                        String productImage, String categoryName, boolean checked) {
        this.id = id;
        this.productName = productName;
        this.productImage = productImage;
        this.categoryName = categoryName;
        this.checked = checked;
    }

    public long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}

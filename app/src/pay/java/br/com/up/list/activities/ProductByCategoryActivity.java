package br.com.up.list.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;
import br.com.up.list.R;
import br.com.up.list.adpter.ProductCheckAdapter;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.dialog.DialogAddProductsChecked;
import br.com.up.list.utils.ProductClickListener;
import br.com.up.list.utils.Search;

public class ProductByCategoryActivity extends Activity implements Observer {

    private View layoutSearch;
    private ViewGroup viewGroup;
    private EditText edtSearch;
    private ImageView iconCloseSearch;
    private ListView productList;
    private Button btnAddCheckedProducts;
    private boolean btnAddCheckedProductsVisible;
    private ProductCheckAdapter productCheckAdapter;
    private  long listId;
    private String listName;
    private long categoryId;
    private Context context;
    private List<Long> productsIdsCheckedList = new ArrayList<>();
    private ProductClickListener productClickListener = new ProductClickListener();
    private ProductByCategoryActivity activity;

    // Inicia a atividade MyProductListActivity
    private void startMyProductListActivity(long listId, String listName) {
        Intent intent = new Intent(context, MyProductListActivity.class);
        intent.putExtra("listId", listId);
        intent.putExtra("listName", listName);
        startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_by_category);
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        context = getApplicationContext();
        activity = this;

        productList = findViewById(R.id.products_list);
        layoutSearch = findViewById(R.id.layout_search);
        btnAddCheckedProducts = findViewById(R.id.btn_add_checked_products);
        viewGroup = findViewById(R.id.layout_activity_product_by_category);
        edtSearch = findViewById(R.id.edt_search);
        iconCloseSearch = findViewById(R.id.icon_close_search);

        categoryId = getIntent().getLongExtra("categoryId", 0);
        listId = getIntent().getLongExtra("listId", 0);
        final String categoryName = getIntent().getStringExtra("categoryName");
        listName = getIntent().getStringExtra("listName");

        Objects.requireNonNull(getActionBar()).setTitle(categoryName);

        Toast msgException = Toast.makeText(
                context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        Cursor cursorProducts = sqLiteHelper.getProductsByCategory(categoryId, msgException);

        ProductCheckAdapter.setCheckVisibility(View.INVISIBLE);
        ProductCheckAdapter.setProductCheckedList(new ArrayList<>());
        productCheckAdapter = new ProductCheckAdapter(context, cursorProducts, 0);

        productList.setAdapter(productCheckAdapter);
        productClickListener.setOnItemClick(activity, productList, listId, listName);
        productCheckAdapter.getObservable().addObserver(this);

        edtSearch.addTextChangedListener(Search.filter(sqLiteHelper, categoryId, msgException, productList, this));
        iconCloseSearch.setOnClickListener(Search.close(viewGroup, layoutSearch));

        btnAddCheckedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listId > 0) {
                    int messageId = sqLiteHelper.insertProductsList(
                            productsIdsCheckedList, listId, msgException
                    );

                    Toast.makeText(context, messageId, Toast.LENGTH_LONG).show();
                    startMyProductListActivity(listId, listName);

                } else {
                    new DialogAddProductsChecked(
                            activity, sqLiteHelper, getLayoutInflater(), getResources(),
                            productsIdsCheckedList, msgException
                    ).onCreateDialogList().show();
                }
            }
        });
    }

    // Abilita/Desabilita botão para adicionar os produtos com check
    private void changeBtnAddCheckedProducts() {
        if (!btnAddCheckedProductsVisible) {
            btnAddCheckedProductsVisible = true;
            btnAddCheckedProducts.setVisibility(View.VISIBLE);
        } else {
            btnAddCheckedProductsVisible = false;
            btnAddCheckedProducts.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.products_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_search:
                Search.open(viewGroup, layoutSearch);
                break;
            case R.id.item_select:
                ProductCheckAdapter.changeCheckVisible();
                productClickListener.setOnItemClick(activity, productList, listId, listName);
                changeBtnAddCheckedProducts();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable o, Object productsIdsCheckedList) {
        this.productsIdsCheckedList = (List<Long>) productsIdsCheckedList;
        int countProductsChecked = this.productsIdsCheckedList.size();

        StringBuilder countProduct = new StringBuilder();
        countProduct.append(getResources().getString(R.string.btn_products_add_checked));
        countProduct.append(" (");
        countProduct.append(countProductsChecked);
        countProduct.append(")");
        btnAddCheckedProducts.setText(countProduct.toString());
    }

}

package br.com.up.list.utils;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import br.com.up.list.R;
import br.com.up.list.activities.ProductDetail;

public class ProductClickListener {
    private boolean activeItemClick = false;

    public void setOnItemClick(Context context, ListView productList, long listId, String listName) {
        if (activeItemClick) {
            activeItemClick = false;
            productList.setOnItemClickListener(null);
        } else {
            activeItemClick = true;
            productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView,
                                        View view, int productPosition, long productId) {

                    Intent intent = new Intent(context, ProductDetail.class);
                    TextView txtProductName = view.findViewById(R.id.product_name);
                    intent.putExtra("productId", productId);
                    intent.putExtra("listId", listId);
                    intent.putExtra("listName", listName);
                    intent.putExtra("productName", txtProductName.getText().toString());

                    context.startActivity(intent);
                }
            });
        }
    }
}

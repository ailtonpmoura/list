package br.com.up.list.fragments;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import br.com.up.list.activities.ProductByCategoryActivity;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;


public class CategoryFragment extends Fragment {

    private Context context;
    private View fragmentView;
    private SQLiteHelper sqLiteHelper;

    public CategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.activity_category,
                container,
                false
        );
    }

    @Override
    public void onStart() {
        super.onStart();

        ListView categoryList = fragmentView.findViewById(R.id.category_list);
        Toast msgException = Toast.makeText(context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        categoryList.setAdapter(new SimpleCursorAdapter(
                context,
                android.R.layout.simple_list_item_1,
                sqLiteHelper.getCategories(msgException),
                new String[] {"NAME"},
                new int[] {android.R.id.text1},
                0
        ));

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long categoryId) {
                Intent intent = new Intent(context, ProductByCategoryActivity.class);
                TextView txtCategoryName = view.findViewById(android.R.id.text1);
                intent.putExtra("categoryId", categoryId);
                intent.putExtra("listId", 0);
                intent.putExtra("categoryName", txtCategoryName.getText().toString());
                startActivity(intent);
            }
        });
    }
}

package br.com.up.list.fragments;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.up.list.R;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.utils.CustomCursorAdapter;

public class MyCreatedProductsFragment extends Fragment {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private View fragmentView;
    private ListView listMyCreatedProduct;

    public MyCreatedProductsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = inflater.getContext();
        this.sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_my_created_products,
                container,
                false);
    }

    @Override
    public void onStart() {
        super.onStart();

        listMyCreatedProduct = fragmentView.findViewById(R.id.list_my_created_products);
        TextView myCreatedProductEmpty = fragmentView.findViewById(R.id.my_created_product_list_empty);

        Toast msgException = Toast.makeText(
                context,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT
        );

        Cursor cursorMyCreatedProducts = sqLiteHelper.getMyCreatedProducts(msgException);

        if(cursorMyCreatedProducts.moveToFirst()) {
            myCreatedProductEmpty.setVisibility(View.GONE);
            listMyCreatedProduct.setAdapter(new CustomCursorAdapter(
                    context,
                    cursorMyCreatedProducts,
                    0
            ));

        } else {
            myCreatedProductEmpty.setVisibility(View.VISIBLE);
        }
    }
}

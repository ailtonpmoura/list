package br.com.up.list.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Calendar;
import br.com.up.list.activities.MyProductListActivity;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.R;

public class ListFragment extends Fragment {

    private Context context;
    private View fragmentView;
    private TextView listEmpty;
    private ListView listSaved;
    private SQLiteHelper sqLiteHelper;
    private Toast msgException;

    public ListFragment() {
    }

    private void startActivity(long listId, String listName) {
        Intent intent = new Intent(context, MyProductListActivity.class);
        intent.putExtra("listId", listId);
        intent.putExtra("listName", listName);
        startActivity(intent);
    }

    private void openToast(int idMsg) {
        Toast.makeText(context, getResources().getString(idMsg), Toast.LENGTH_SHORT).show();
    }

    private void inactiveList(long listId) {
        sqLiteHelper.updateActiveList(listId, 0);
        Cursor cursor = sqLiteHelper.getLists(1, msgException);
        CursorAdapter listAdapter = (CursorAdapter) listSaved.getAdapter();
        listAdapter.changeCursor(cursor);
        openToast(R.string.list_excluded);

        // Se não houver registros no cursor, desabilita a lista e o adaptador
        if(!cursor.moveToFirst()) {
            listEmpty.setVisibility(View.VISIBLE);
            listSaved.setAdapter(null);
        }
    }

    private void duplicateList(long listIdToDuplicity, View viewListItem) {
        long newListId;
        TextView txtListNameToDuplicity = viewListItem.findViewById(R.id.list_name);
        String listNameToDuplicity = txtListNameToDuplicity.getText().toString();
        saveOrUpdateList("createList", listNameToDuplicity, 0);

        Cursor listsActiviesCursor = sqLiteHelper.getLists(1, msgException);
        Cursor lastListCursor = sqLiteHelper.getLastList(msgException);
        Cursor productsListCursor = sqLiteHelper.getProductsList(listIdToDuplicity, msgException);

        if (lastListCursor.moveToFirst()) {
            newListId = lastListCursor.getLong(0);

            while (productsListCursor.moveToNext()) {
                long productId = productsListCursor.getLong(0);
                Cursor productDetailCursor = sqLiteHelper.getProductDetail(
                        listIdToDuplicity, productId, msgException);

                if (productDetailCursor.moveToFirst()) {
                    sqLiteHelper.addProductList(newListId, productId, msgException);
                    sqLiteHelper.addProductDetail(
                            Double.parseDouble(productDetailCursor.getString(4).replace(",", ".")),
                            productDetailCursor.getInt(5),
                            productDetailCursor.getString(6),
                            Double.parseDouble(productDetailCursor.getString(7).replace(",", ".")),
                            newListId,
                            productId,
                            msgException
                    );
                }
            }

            if (listsActiviesCursor.moveToFirst()) {
                CursorAdapter listSavedAdpter = (CursorAdapter) listSaved.getAdapter();
                listSavedAdpter.changeCursor(listsActiviesCursor);
                openToast(R.string.list_duplicity);
            }
        }
    }

    private void createPopUpMenu(final View viewListItem, final long listId) {
        PopupMenu popupMenu = new PopupMenu(context, viewListItem.findViewById(R.id.list_name));
        popupMenu.inflate(R.menu.menu_list);
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_list_edit :
                        onCreateDialog("updateList", viewListItem, listId).show();
                        break;
                    case R.id.menu_list_duplicity :
                        duplicateList(listId, viewListItem);
                        break;
                    case R.id.menu_list_delete :
                        inactiveList(listId);
                        break;
                }
                return true;
            }
        });
    }

    private void setAdapterAndClickListener(Cursor cursor) {
        if(cursor.moveToFirst()) {
            listEmpty.setVisibility(View.GONE);

            listSaved.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_list,
                    cursor,
                    new String[] {"NAME", "DATE"},
                    new int[] {R.id.list_name, R.id.list_date},
                    0
            ));

            listSaved.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(
                        AdapterView<?> parent, View view, int position, long listId) {
                    TextView txtListName = view.findViewById(R.id.list_name);
                    String listName = txtListName.getText().toString();
                    startActivity(listId, listName);
                }
            });

            listSaved.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(
                        AdapterView<?> parent, View view, int position, long listId) {
                    createPopUpMenu(view, listId);
                    return true;
                }
            });

        } else {
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    private int saveOrUpdateList(String action, String listName, long listId) {
        if (action.equals("createList")) {
            sqLiteHelper.saveList(listName, Calendar.getInstance().getTimeInMillis());
            return R.string.list_created;
        } else {
            sqLiteHelper.updataeNameList(listId, listName);
            return R.string.list_updated;
        }
    }

    private boolean checkField(String listName) {
        if(listName.equals("")) {
            openToast(R.string.check_name_list);
            return false;
        }
        return true;
    }

    private void setTextDialog(TextView txtMessageDialog, int idMessage) {
        txtMessageDialog.setText(getResources().getString(idMessage));
    }

    private int configMessagesDialog(
            EditText edtListName, TextView txtMessageDialog, View viewListItem, String action) {
        if (action.equals("createList")) {
            setTextDialog(txtMessageDialog, R.string.name_list);
            return R.string.create;
        } else {
            setTextDialog(txtMessageDialog, R.string.new_name_list);
            edtListName.setText(((TextView) viewListItem.findViewById(R.id.list_name)).getText());
            return R.string.update_list;
        }
    }

    private Dialog onCreateDialog(final String action, View viewListItem, final long listId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View viewDialog = inflater.inflate(R.layout.create_list_dialog_layout, null);
        final TextView txtMessageDialog = viewDialog.findViewById(R.id.text_message_dialog);
        final EditText edtListName = viewDialog.findViewById(R.id.text_list_name);
        builder.setView(viewDialog);

        // Botão da caixa de diálogo de confirmação
        builder.setPositiveButton(
                configMessagesDialog(edtListName, txtMessageDialog, viewListItem, action),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String listName = edtListName.getText().toString();
                        if(checkField(listName)) {
                            int idMessageToast = saveOrUpdateList(action, listName, listId);
                            Cursor cursor = sqLiteHelper.getLists(1, msgException);

                            // Verifica se já existe um adapter na lista
                            if(listSaved.getAdapter() != null) {
                                CursorAdapter listAdapter = (CursorAdapter) listSaved.getAdapter();
                                listAdapter.changeCursor(cursor);
                            } else {
                                setAdapterAndClickListener(cursor);
                            }
                            openToast(idMessageToast);
                        }
                    }
                });

        // Botão do diálogo de negação
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = inflater.getContext();
        sqLiteHelper = new SQLiteHelper(context);
        return fragmentView = inflater.inflate(
                R.layout.fragment_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        listEmpty = fragmentView.findViewById(R.id.list_empty);
        listSaved = fragmentView.findViewById(R.id.list_saved);
        Button btnAddList = fragmentView.findViewById(R.id.btn_add_list);

        msgException = Toast.makeText(
                context, getResources().getString(R.string.exception_search), Toast.LENGTH_SHORT);

        Cursor cursor = sqLiteHelper.getLists(1, msgException);
        setAdapterAndClickListener(cursor);

        btnAddList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog("createList", null, 0).show();
            }
        });
    }
}

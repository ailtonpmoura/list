package br.com.up.list.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Date;
import java.util.List;
import br.com.up.list.R;
import br.com.up.list.activities.MyProductListActivity;
import br.com.up.list.database.SQLiteHelper;

public class DialogAddProductsChecked {

    private Context context;
    private SQLiteHelper sqLiteHelper;
    private LayoutInflater inflater;
    private Resources resources;
    private List<Long> productsIdsCheckedList;
    private Toast msgException;

    public DialogAddProductsChecked(Context context, SQLiteHelper sqLiteHelper,
                                    LayoutInflater inflater, Resources resources,
                                    List<Long> productsIdsCheckedList, Toast msgException) {
        this.context = context;
        this.sqLiteHelper = sqLiteHelper;
        this.inflater = inflater;
        this.resources =  resources;
        this.productsIdsCheckedList = productsIdsCheckedList;
        this.msgException = msgException;
    }

    private void startMyProductListActivity(long listId, String listName) {
        Intent intent = new Intent(context, MyProductListActivity.class);
        intent.putExtra("listId", listId);
        intent.putExtra("listName", listName);
        context.startActivity(intent);
    }

    // Caixa de diálogo para criar uma nova lista
    public Dialog onCreateDialogCreateList() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogCreateList = inflater.inflate(R.layout.create_list_dialog_layout, null);
        final TextView textMessageDialog = dialogCreateList.findViewById(R.id.text_message_dialog);
        final EditText textListName = dialogCreateList.findViewById(R.id.text_list_name);
        textMessageDialog.setText(resources.getString(R.string.name_list));
        builder.setView(dialogCreateList);

        builder.setPositiveButton(
                resources.getString(R.string.create),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String listName = textListName.getText().toString();
                        if(!listName.equals("")) {
                            sqLiteHelper.saveList(listName, new Date().getTime());
                            onCreateDialogList().show();
                            Toast.makeText(context, R.string.list_created, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.check_name_list, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        builder.setNegativeButton(
                resources.getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        return builder.create();
    }

    // Caixa de diálogo para visualizar a lista e adicionar o produto na lista selecionada
    public Dialog onCreateDialogList() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogCreateList = inflater.inflate(R.layout.view_list_dialog_layout, null);
        final ListView listDialog = dialogCreateList.findViewById(R.id.list_dialog);
        TextView listEmpty = dialogCreateList.findViewById(R.id.txt_list_empty);
        TextView dialogMessage = dialogCreateList.findViewById(R.id.txt_dialog_message);
        builder.setView(dialogCreateList);

        // Busca as listas Ativas
        Cursor cursorListActive = sqLiteHelper.getLists(1, msgException);

        if(cursorListActive.moveToFirst()) {
            dialogMessage.setVisibility(View.VISIBLE);
            listEmpty.setVisibility(View.GONE);

            listDialog.setAdapter(new SimpleCursorAdapter(
                    context,
                    R.layout.item_custom_list_dialog,
                    cursorListActive,
                    new String[]{"NAME"},
                    new int[]{R.id.list_name},
                    0
            ));

            builder.setPositiveButton(
                    resources.getString(R.string.add),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (listDialog.getCheckedItemIds().length > 0) {
                                long listId = listDialog.getCheckedItemIds()[0];

                                Cursor cursorListName = sqLiteHelper.getListName(listId, msgException);

                                if (cursorListName.moveToFirst()) {
                                    String listName = cursorListName.getString(1);

                                    int messageId = sqLiteHelper.insertProductsList(
                                            productsIdsCheckedList, listId, msgException
                                    );

                                    Toast.makeText(context, messageId, Toast.LENGTH_LONG).show();
                                    startMyProductListActivity(listId, listName);

                                } else {
                                    msgException.show();
                                }
                            } else {
                                Toast.makeText(context, R.string.msg_select_list, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        } else {
            listEmpty.setVisibility(View.VISIBLE);
            dialogMessage.setVisibility(View.GONE);
        }

        builder.setNegativeButton(
                resources.getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        // Inicializa a caixa de diálogo para criar uma nova lista
        builder.setNeutralButton(
                resources.getString(R.string.create),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onCreateDialogCreateList().show();
                    }
                }
        );

        return builder.create();
    }
}

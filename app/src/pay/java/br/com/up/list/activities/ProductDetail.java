package br.com.up.list.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
/*import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;*/
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/*import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;*/

import java.util.Date;
import br.com.up.list.database.SQLiteHelper;
import br.com.up.list.utils.Mask;
import br.com.up.list.R;

public class ProductDetail extends Activity {
    private double productPrice;
    private double productAmount;
    private double productTotal;
    private long productId;
    private long listId;
    private boolean isUpdate;
    private String productUnity;
    private EditText txtProductPrice;
    private EditText txtProductAmount;
    private ImageView productImage;
    private Spinner spnProductUnity;
    private TextView txtProductPriceTotal;
    private Switch isFavorite;
    private Button btnAddProduct;
    private Button btnUpdateProduct;
    private String productName;
    private String listName;
    private Toast msgException;
    private SQLiteHelper sqLiteHelper;

    // Seta os detalhes do produto
    private void setProductDetails(Spinner spnProductUnity,
                                   EditText txtProductPrice,
                                   EditText txtProductAmount) {
        productUnity = spnProductUnity.getSelectedItem().toString();
        productPrice = Double.parseDouble(txtProductPrice.getText().toString().replace(",","."));
        productAmount = Double.parseDouble(txtProductAmount.getText().toString());
        productTotal = productPrice * productAmount;
    }

    // Método chamado pelo Layout da View Switch
    public void isFavorite(View view) {
        if(((Switch)view).isChecked()) {
            sqLiteHelper.updateFavorite(productId,1);
        } else {
            sqLiteHelper.updateFavorite(productId, 0);
        }
    }

    // Verifica se os campos estão preenchidos
    private boolean checkFields(EditText edtPrice, EditText edtAmount) {
        String price = edtPrice.getText().toString();
        String amount = edtAmount.getText().toString();
        return !price.equals("") && !amount.equals("");
    }

    // Exibe mensagens
    private void showToast(int idMsg) {
        Toast.makeText(
                getApplicationContext(),
                getResources().getString(idMsg),
                Toast.LENGTH_LONG)
                .show();
    }

    // Inicia a atividade MyProductListActivity
    private void startMyProductListActivity(long listId, String listName) {
        Intent intent = new Intent(getApplicationContext(), MyProductListActivity.class);
        intent.putExtra("listId", listId);
        intent.putExtra("listName", listName);
        startActivity(intent);
    }

    // Adiciona o produto na lista selecionada
    private boolean addProductList () {
        if(listId > 0) {
            Cursor productCursor = sqLiteHelper.verifyProductToList(listId, productId);
            if(!productCursor.moveToFirst()) {
                if (checkFields(txtProductPrice, txtProductAmount)) {
                    if (!txtProductAmount.getText().toString().equals(".")) {
                        setProductDetails(spnProductUnity, txtProductPrice, txtProductAmount);
                        sqLiteHelper.addProductList(listId, productId, msgException);
                        sqLiteHelper.addProductDetail(productPrice, productAmount,
                                productUnity, productTotal, listId, productId, msgException);
                    } else {
                        showToast(R.string.check_amount);
                        return false;
                    }
                } else {
                    showToast(R.string.check_fields);
                    return false;
                }
            } else {
                showToast(R.string.duplicate_product);
                return false;
            }
        } else {
            onCreateDialogList().show();
            return false;
        }
        return true;
    }

    // Atualiza o produto
    private boolean updateProductList() {
        if (checkFields(txtProductPrice, txtProductAmount)) {
            setProductDetails(spnProductUnity, txtProductPrice, txtProductAmount);
            sqLiteHelper.updateProductDetail(
                    productPrice,
                    productAmount,
                    productUnity,
                    productTotal,
                    listId,
                    productId
            );
        } else {
            showToast(R.string.check_fields);
            return false;
        }
        return true;
    }

    // Adiciona os detalhes básico do produto
    private void basicProductDetail(Cursor productCursor, int visibleBtnAdd, int visibleBtnUpdate) {
        if(productCursor.getInt(3) == 1) {
            isFavorite.setChecked(true);
        }
        btnAddProduct.setVisibility(visibleBtnAdd);
        btnUpdateProduct.setVisibility(visibleBtnUpdate);

        String imageResource = productCursor.getString(2);

        if (imageResource.contains("/")) {
            productImage.setImageURI(Uri.parse(imageResource));
        } else {
            productImage.setImageResource(Integer.parseInt(imageResource));
        }
    }

    // Adiciona todos os detalhes do produto
    private void completeProductDetail(Cursor detailProductCursor) {
        String price = detailProductCursor.getString(4);
        String amount = detailProductCursor.getString(5);
        String unity = detailProductCursor.getString(6);
        String total = detailProductCursor.getString(7);
        String[] unities = getResources().getStringArray(R.array.unity);

        txtProductPrice.setText(price);
        txtProductAmount.setText(amount);
        txtProductPriceTotal.setText(total);

        txtProductPrice.setSelection(txtProductPrice.getText().length());
        txtProductAmount.setSelection(txtProductAmount.getText().length());

        for (int i = 0; i < unities.length; i++) {
            if (unities[i].equals(unity)) {
                spnProductUnity.setSelection(i);
            }
        }
    }

    // Adiciona máscara nos campos monetários
    private void addMaskMoney() {
        txtProductPrice.addTextChangedListener(Mask.formatAndcalculate(
                txtProductPrice,
                txtProductAmount,
                txtProductPriceTotal
        ));

        txtProductAmount.addTextChangedListener(Mask.calculate(
                txtProductPrice,
                txtProductAmount,
                txtProductPriceTotal
        ));
    }

    // Configura a atividade para atualização ou novo produto
    private void configActivity() {
        if(isUpdate) {
            Cursor detailProductCursor = sqLiteHelper.getProductDetail(listId, productId, msgException);
            if(detailProductCursor.moveToFirst()) {
                basicProductDetail(detailProductCursor, View.GONE, View.VISIBLE);
                completeProductDetail(detailProductCursor);

                btnUpdateProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(updateProductList()) {
                            startMyProductListActivity(listId, listName);
                            showToast(R.string.product_update);
                        }
                    }
                });
            }
        } else {
            Cursor productCursor = sqLiteHelper.getProduct(productId, msgException);
            if(productCursor.moveToFirst()) {
                basicProductDetail(productCursor, View.VISIBLE, View.GONE);

                btnAddProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(addProductList()) {
                            startMyProductListActivity(listId, listName);
                            showToast(R.string.msg_product_add);
                        }
                    }
                });
            }
        }
    }

    // Caixa de diálogo para criar uma nova lista
    private Dialog onCreateDialogCreateList() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = getLayoutInflater();
        final View dialogCreateList = inflater.inflate(R.layout.create_list_dialog_layout, null);
        final TextView textMessageDialog = dialogCreateList.findViewById(R.id.text_message_dialog);
        final EditText textListName = dialogCreateList.findViewById(R.id.text_list_name);
        textMessageDialog.setText(getResources().getString(R.string.name_list));
        builder.setView(dialogCreateList);

        // botão da caixa de diálogo de Criação de lista
        builder.setPositiveButton(
                getResources().getString(R.string.create),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String listName = textListName.getText().toString();
                        if(!listName.equals("")) {
                            sqLiteHelper.saveList(listName, new Date().getTime());
                            onCreateDialogList().show();
                            showToast(R.string.list_created);
                        } else {
                            showToast(R.string.check_name_list);
                        }
                    }
                }
        );

        // Botão do diálogo de negação
        builder.setNegativeButton(
                getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        return builder.create();
    }

    /*@Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(reqCode, resultCode, data);
        if (result != null) {
            String barcode = result.getContents();
            if (barcode != null && !barcode.equals("")) {
                txtProductPrice.setText(barcode);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.products_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_search:
                IntentIntegrator intent = new IntentIntegrator(this);
                intent.initiateScan();
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/

    // Caixa de diálogo para visualizar a lista e adicionar o produto na lista selecionada
    private Dialog onCreateDialogList() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = getLayoutInflater();
        final View dialogCreateList = inflater.inflate(R.layout.view_list_dialog_layout, null);
        final ListView listDialog = dialogCreateList.findViewById(R.id.list_dialog);
        TextView listEmpty = dialogCreateList.findViewById(R.id.txt_list_empty);
        TextView dialogMessage = dialogCreateList.findViewById(R.id.txt_dialog_message);
        builder.setView(dialogCreateList);

        // Busca as listas Ativas
        Cursor cursorListActive = sqLiteHelper.getLists(1, msgException);
        if(cursorListActive.moveToFirst()) {
            dialogMessage.setVisibility(View.VISIBLE);
            listEmpty.setVisibility(View.GONE);

            listDialog.setAdapter(new SimpleCursorAdapter(
                    getApplicationContext(),
                    R.layout.item_custom_list_dialog,
                    cursorListActive,
                    new String[]{"NAME"},
                    new int[]{R.id.list_name},
                    0
            ));

            // Botão para adicionar o produto na lista selecionada
            builder.setPositiveButton(
                    getResources().getString(R.string.add),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (listDialog.getCheckedItemIds().length > 0) {
                                listId = listDialog.getCheckedItemIds()[0];

                                if(addProductList()) {
                                    Cursor cursorListName = sqLiteHelper.getListName(listId, msgException);
                                    if (cursorListName.moveToFirst()) {
                                        listName = cursorListName.getString(1);
                                        startMyProductListActivity(listId, listName);
                                        showToast(R.string.msg_product_add);
                                    }
                                } else {
                                    listId = 0;
                                }
                            } else {
                                showToast(R.string.msg_select_list);
                            }
                        }
                    });
        } else {
            listEmpty.setVisibility(View.VISIBLE);
            dialogMessage.setVisibility(View.GONE);
        }

        // Botão do diálogo de negação
        builder.setNegativeButton(
                getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        // Inicializa a caixa de diálogo para criar uma nova lista
        builder.setNeutralButton(
                getResources().getString(R.string.create),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onCreateDialogCreateList().show();
                    }
                }
        );

        return builder.create();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        sqLiteHelper = new SQLiteHelper(this);

        txtProductPrice = findViewById(R.id.edt_product_price);
        txtProductAmount = findViewById(R.id.edt_product_amount);
        spnProductUnity = findViewById(R.id.spn_product_unity);
        productImage = findViewById(R.id.product_image);
        isFavorite = findViewById(R.id.product_favorite);
        txtProductPriceTotal = findViewById(R.id.edt_product_price_total);
        btnAddProduct = findViewById(R.id.btn_add_product);
        btnUpdateProduct = findViewById(R.id.btn_update_product);

        productId = getIntent().getLongExtra("productId", 0);
        listId = getIntent().getLongExtra("listId", 0);
        productName = getIntent().getStringExtra("productName");
        listName = getIntent().getStringExtra("listName");
        isUpdate = getIntent().getBooleanExtra("isUpdate", false);

        txtProductPrice.setSelection(txtProductPrice.getText().length());
        txtProductAmount.setSelection(txtProductAmount.getText().length());

        configActivity();
        addMaskMoney();
        getActionBar().setTitle(productName);

        msgException = Toast.makeText(
                this,
                getResources().getString(R.string.exception_search),
                Toast.LENGTH_SHORT);
    }
}

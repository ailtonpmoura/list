package br.com.up.list.adpter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import br.com.up.list.R;
import br.com.up.list.entity.ProductCheck;
import br.com.up.list.utils.ObservableUtil;

public class ProductCheckAdapter extends CursorAdapter {

    private static ObservableUtil observable = new ObservableUtil();
    private LayoutInflater layoutInflater;
    private  static List<Long> productsIdsCheckedList = new ArrayList<>();
    private List<ProductCheck> productCheckedList = new ArrayList<>();
    private static List<View> viewList;
    private View view;
    private static int checkVisibility = View.INVISIBLE;

    // Constructor
    public ProductCheckAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewList = new ArrayList<>();

        while (cursor.moveToNext()) {
            final long productId = cursor.getLong(0);
            String productName = cursor.getString(1);
            String productImage = cursor.getString(2);
            String categoryName = cursor.getString(4);

            ProductCheck productCheck = new ProductCheck(
                    productId, productName, productImage, categoryName, false
            );

            if (productsIdsCheckedList.size() > 0) {
                if (productsIdsCheckedList.contains(productCheck.getId())) {
                    productCheck.setChecked(true);
                }
            }

            productCheckedList.add(productCheck);
        }
    }

    public static void changeCheckVisible() {
        if (checkVisibility == View.INVISIBLE) {
            checkVisibility = View.VISIBLE;
        } else {
            checkVisibility = View.INVISIBLE;
        }

        for (View view : viewList) {
            view.findViewById(R.id.product_check).setVisibility(checkVisibility);
        }
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        TextView productIdView = view.findViewById(R.id.product_id);
        ImageView productImageView = view.findViewById(R.id.product_image);
        TextView productNameView = view.findViewById(R.id.product_name);
        CheckBox productCheckView = view.findViewById(R.id.product_check);

        ProductCheck productCheck = productCheckedList.get(cursor.getPosition());
        Long productId = productCheck.getId();
        String productName = productCheck.getProductName();
        String productImage = productCheck.getProductImage();
        boolean productIsCheck = productCheck.isChecked();

        productIdView.setText(productId.toString());
        productNameView.setText(productName);
        productCheckView.setChecked(productIsCheck);
        productCheckView.setVisibility(checkVisibility);

        if (productImage.contains("/")) {
            productImageView.setImageURI(Uri.parse(productImage));
        } else {
            productImageView.setImageResource(Integer.parseInt(productImage));
        }

        productCheckView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!productCheck.isChecked()) {
                    productCheckView.setChecked(true);
                    productCheck.setChecked(true);
                    productsIdsCheckedList.add(productCheck.getId());
                    observable.setChanged();
                    observable.notifyObservers(productsIdsCheckedList);
                } else {
                    productCheckView.setChecked(false);
                    productCheck.setChecked(false);
                    productsIdsCheckedList.remove(productCheck.getId());
                    observable.setChanged();
                    observable.notifyObservers(productsIdsCheckedList);
                }
            }
        });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        view = layoutInflater.inflate(R.layout.item_custom_product, parent, false);
        viewList.add(view);
        return view;
    }
    
    public static void setCheckVisibility(int visibility) {
        ProductCheckAdapter.checkVisibility = visibility;
    }

    public static void setProductCheckedList(List<Long> productCheckedList) {
        ProductCheckAdapter.productsIdsCheckedList = productCheckedList;
    }

    public Observable getObservable() {
        return observable;
    }
}

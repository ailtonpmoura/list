package br.com.up.list.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import br.com.up.list.R;
import br.com.up.list.activities.MainActivity;
import br.com.up.list.database.SQLiteHelper;

public class CustomCursorAdapter extends CursorAdapter{
    private LayoutInflater inflater;

    // Default constructor
    public CustomCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void openToast(int idMsg, Context context) {
        Toast.makeText(context, context.getResources().getString(idMsg), Toast.LENGTH_SHORT).show();
    }

    private void deleteProductAndUpdateCursor(Context context, SQLiteHelper sqLiteHelper,
                                              long productId, ListView listView,
                                              Toast msgException) {
        sqLiteHelper.deleteMyCreatedProduct(productId);
        Cursor myCreatedProductCursor = sqLiteHelper.getMyCreatedProducts(msgException);
        ((CursorAdapter) listView.getAdapter()).changeCursor(myCreatedProductCursor);

        if (!myCreatedProductCursor.moveToFirst()) {
            TextView myCreatedProduct = ((View) listView.getParent()).findViewById(R.id.my_created_product_list_empty);
            myCreatedProduct.setVisibility(View.VISIBLE);
        }

        Toast.makeText(context, R.string.product_excluded, Toast.LENGTH_SHORT).show();
    }

    private Dialog onCreateDialog(final Context context, final long productId, final ListView listView) {
        final SQLiteHelper sqLiteHelper = new SQLiteHelper(context);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.msg_delete_product);

        final Toast msgException = Toast.makeText(
                context,
                R.string.exception_search,
                Toast.LENGTH_SHORT
        );

        // Botão da caixa de diálogo de confirmação
        builder.setPositiveButton(
                R.string.confirm,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Cursor myProductCursor = sqLiteHelper.getMyCreatedProduct(productId, msgException);

                        if (myProductCursor.moveToFirst()) {
                            String imageUri = myProductCursor.getString(2);

                            if (imageUri.contains("/")) {
                                FileInputStream inputStream = null;
                                File file;

                                try {
                                    inputStream = new FileInputStream(imageUri);
                                    file = new File(imageUri);
                                    file.delete();
                                    deleteProductAndUpdateCursor(
                                            context, sqLiteHelper,
                                            productId, listView,
                                            msgException
                                    );

                                } catch (FileNotFoundException e) {
                                    openToast(R.string.error_delete_product, context);
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        inputStream.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                deleteProductAndUpdateCursor(
                                        context, sqLiteHelper,
                                        productId, listView,
                                        msgException
                                );
                            }
                        }
                    }
                });

        // Botão do diálogo de negação
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        final long productId = cursor.getLong(0);
        String productName = cursor.getString(1);
        String productImage = cursor.getString(2);
        String categoryName = cursor.getString(3);

        ImageView productImageView = view.findViewById(R.id.img_created_product_custom);
        TextView productNameView = view.findViewById(R.id.txt_created_product_name_custom);
        TextView categoryNameView = view.findViewById(R.id.txt_created_product_category_custom);

        if (productImage.contains("/")) {
            productImageView.setImageURI(Uri.parse(productImage));
        } else {
            productImageView.setImageResource(Integer.parseInt(productImage));
        }

        productNameView.setText(productName);
        categoryNameView.setText(categoryName);

        ImageButton btnUpdateProduct = view.findViewById(R.id.btn_update_created_product_custom);
        ImageButton btnDeleteProduct =  view.findViewById(R.id.btn_delete_created_product_custom);

        btnUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("productEdit", true);
                intent.putExtra("productId", productId);
                context.startActivity(intent);
            }
        });

        btnDeleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog(context, productId, (ListView) view.getParent()).show();
            }
        });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.item_custom_created_product, parent, false);
    }
}

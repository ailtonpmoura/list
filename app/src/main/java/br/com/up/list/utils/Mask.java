package br.com.up.list.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public final class Mask {

    private Mask() {};

    public static String unmask(String s) {
        return s.replaceAll("[.]", "").replaceAll("[-]", "")
                .replaceAll("[/]", "").replaceAll("[(]", "")
                .replaceAll("[)]", "");
    }

    public static TextWatcher insert(final String mask, final EditText ediTxt) {
        return new TextWatcher() {
            boolean isUpdating;
            String old = "";

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                String str = Mask.unmask(s.toString());
                String mascara = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (char m : mask.toCharArray()) {
                    if (m != '#' && str.length() > old.length()) {
                        mascara += m;
                        continue;
                    }
                    try {
                        mascara += str.charAt(i);
                    } catch (Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                ediTxt.setText(mascara);
                ediTxt.setSelection(mascara.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };
    }

    public static TextWatcher calculate(final EditText txtProductPrice, final EditText txtProductAmount, final TextView txtProductPriceTotal) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                calculateTotal(txtProductPrice, txtProductAmount, txtProductPriceTotal);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    public static TextWatcher formatAndcalculate(final EditText txtProductPrice, final EditText txtProductAmount, final TextView txtProductPriceTotal) {
        return new TextWatcher() {
            // Mascara monetaria para o preço do produto
            private String current = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    txtProductPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[^0-9]", "");
                    double parsed = Double.parseDouble(cleanString);

                    String formatted = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format((parsed/100));
                    current = formatted.replaceAll("[^0-9.,]", "");

                    txtProductPrice.setText(current.trim());
                    txtProductPrice.setSelection(current.length());

                    calculateTotal(txtProductPrice, txtProductAmount, txtProductPriceTotal);

                    txtProductPrice.addTextChangedListener(this);
                }
            }

        };
    }

    private static void calculateTotal(final EditText txtProductPrice, final EditText txtProductAmount, final TextView txtProductPriceTotal) {
        String producPrice = txtProductPrice.getText().toString();
        String productAmount = txtProductAmount.getText().toString();

        if(!producPrice.equals("") && !productAmount.equals("") && !productAmount.equals(".")) {

            double producPriceParsed = Double.parseDouble(producPrice.replaceAll("[^0-9]", ""));
            double amount = Double.parseDouble(txtProductAmount.getText().toString());

            Double total = producPriceParsed * amount;
            String totalFormated = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format((total/100));
            txtProductPriceTotal.setText(totalFormated.replaceAll("[^0-9.,]", ""));

        } else {
            txtProductPriceTotal.setText("0,00");
        }
    }

}